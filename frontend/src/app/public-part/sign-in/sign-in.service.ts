import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })
export class SignService {
  constructor(private readonly http: HttpClient) {}

  // the request return -> { msg: "User created!" }, not ShowUserDTO
  registerUser(username: string, password: string): Observable<any> {
    return this.http.post<any>(`http://localhost:3000/users`, {
      name: username,
      password,
    });
  }
}
