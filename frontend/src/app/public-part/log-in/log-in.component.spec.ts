import { MaterialModule } from "../../material/material.module";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { LogInComponent } from "./log-in.component";
import { LoginService } from "./log-in.service";
import { StorageService } from "./storage.service";
import { SharedService } from "../shared.service";
import { of } from "rxjs";
import { ComponentFixture, TestBed, async } from "@angular/core/testing";

describe("LogInComponent", () => {
  let loginService: any;
  let storage: any;
  let sharedService: any;

  let component: LogInComponent;
  let fixture: ComponentFixture<LogInComponent>;

  beforeEach(async(() => {
    jest.clearAllMocks();

    loginService = {
      loginUser() {
        // empty
      },
    };

    storage = {
      setItem() {
        // empty
      },
    };

    sharedService = {
      storeUser() {
        // empty
      },
    };

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        BrowserModule,
        MaterialModule,
      ],
      declarations: [LogInComponent],
      providers: [LoginService, StorageService, SharedService],
    })
      .overrideProvider(LoginService, { useValue: loginService })
      .overrideProvider(StorageService, { useValue: storage })
      .overrideProvider(SharedService, { useValue: sharedService })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(LogInComponent);
        component = fixture.componentInstance;
      });
  }));

  it("should be defined", () => {
    expect(component).toBeDefined();
  });

  describe("login should", () => {
    it("call loginService.loginUser() once with correect parameters", () => {
      //Arrange
      const mockUsername = "username";
      const mockPassword = "password";

      const spy = jest
        .spyOn(loginService, "loginUser")
        .mockImplementation(() => of(true));

      //Act
      component.login(mockUsername, mockPassword);

      //Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith("username", "password");
    });
  });
});
