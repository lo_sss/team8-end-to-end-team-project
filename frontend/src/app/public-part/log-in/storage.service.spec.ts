import { StorageService } from "./storage.service";
import { TestBed } from "@angular/core/testing";

describe("StorageService", () => {
  let service: StorageService;

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      providers: [StorageService],
    });

    service = TestBed.get(StorageService);
  });

  it("setItem() should call localStorage.setItem", () => {
    jest.spyOn(localStorage, "setItem").mockImplementation(() => {});

    service.setItem("test", "data");

    expect(localStorage.setItem).toHaveBeenCalledTimes(1);
    expect(localStorage.setItem).toHaveBeenCalledWith("test", "data");
  });

  it("getItem() should call localStorage.getItem", () => {
    jest.spyOn(localStorage, "getItem").mockImplementation(() => "");

    service.getItem("test");

    expect(localStorage.getItem).toHaveBeenCalledTimes(1);
    expect(localStorage.getItem).toHaveBeenCalledWith("test");
  });

  it("getItem() should return null if there is no data stored", () => {
    const spy = jest
      .spyOn(localStorage, "getItem")
      .mockImplementation(() => "");

    const result1 = service.getItem("test");

    spy.mockImplementation(() => "undefined");

    const result2 = service.getItem("test");

    expect(result1).toBe(null);
    expect(result2).toBe(null);
  });

  it("removeItem() should call localStorage.removeItem", () => {
    jest.spyOn(localStorage, "removeItem").mockImplementation(() => {});

    service.removeItem("test");

    expect(localStorage.removeItem).toHaveBeenCalledTimes(1);
    expect(localStorage.removeItem).toHaveBeenCalledWith("test");
  });
});
