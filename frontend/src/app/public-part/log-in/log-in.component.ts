import { SharedService } from './../shared.service';
import { StorageService } from './storage.service';
import { LoginService } from './log-in.service';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null,
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css'],
})
export class LogInComponent {
  passwordFormControl = new FormControl('', [Validators.required]);

  usernameFormControl = new FormControl('', [Validators.required]);

  matcher = new MyErrorStateMatcher();

  loginError = false;
  loginSuccess = false;

  constructor(
    private readonly loginService: LoginService,
    private readonly storage: StorageService,
    private readonly sharedService: SharedService,
  ) {}

  login(username: string, password: string): void {
    this.loginService.loginUser(username, password).subscribe(
      response => {
        this.sharedService.isUserLoggedIn$.next(true);
        this.storage.setItem('token', response.token);
        this.sharedService.storeUser(response.user)
        this.loginSuccess = true;
        this.loginError = false;
      },
      ({ error }) => {
        this.loginError = true;
        this.loginSuccess = false;
      },
    );
  }

}
