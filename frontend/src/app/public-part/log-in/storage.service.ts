import { Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })
export class StorageService {
  getItem(key: string) {
    const value = localStorage.getItem(key);
    return value && value !== "undefined" ? value : null;
  }
  setItem(key: string, value: string) {
    return localStorage.setItem(key, value);
  }
  removeItem(key: string) {
    return localStorage.removeItem(key);
  }
}
