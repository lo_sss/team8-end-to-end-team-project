import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class LoginService {
  constructor(private readonly http: HttpClient) {}

  loginUser(username: string, password: string): Observable<any> {
    return this.http.post(`http://localhost:3000/session`, {
      name: username,
      password,
    });
  }
}
