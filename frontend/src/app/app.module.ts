import { NewCommentComponent } from "./posts/new-post/new-comment/new-comment.component";
import { MaterialModule } from "./material/material.module";
import { PageTitleComponent } from "./shared/page-title/page-title.component";
import { MrSmileyComponent } from "./shared/mr-smiley/mr-smiley.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { AppComponent } from "./app.component";
import { ToolbarComponent } from "./toolbar/toolbar.component";
import { AppRoutingModule } from "./app-routing.module";
import { HomepagePublicComponent } from "./public-part/homepage-public/homepage-public.component";
import { MostPopularTopicsComponent } from "./shared/most-popular-topics/most-popular-topics.component";
import { FavouriteTopicsComponent } from "./shared/favourite-topics/favourite-topics.component";
import { FooterComponent } from "./footer/footer.component";
import { SignInComponent } from "./public-part/sign-in/sign-in.component";
import { LogInComponent } from "./public-part/log-in/log-in.component";
import { HomeComponent } from "./home/home.component";
import { CategoryComponent } from "./home/category/category.component";
import { PostWithCommentsComponent } from "./posts/post-with-comments/post-with-comments.component";
import { NewPostComponent } from "./posts/new-post/new-post.component";
import { ProfileComponent } from "./profile/profile.component";
import { AdminComponent } from "./admin/admin.component";
import { AdminMenuComponent } from "./admin/admin-menu/admin-menu.component";
import { SearchResultsComponent } from "./search-results/search-results.component";
import { HttpClientModule } from "@angular/common/http";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CardComponent } from "./home/card/card.component";
import { NgxSmartModalModule } from "ngx-smart-modal";

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    PageTitleComponent,
    HomepagePublicComponent,
    MrSmileyComponent,
    MostPopularTopicsComponent,
    FavouriteTopicsComponent,
    FooterComponent,
    SignInComponent,
    LogInComponent,
    HomeComponent,
    CategoryComponent,
    PostWithCommentsComponent,
    NewPostComponent,
    ProfileComponent,
    AdminComponent,
    AdminMenuComponent,
    SearchResultsComponent,
    CardComponent,
    NewCommentComponent,
  ],
  imports: [
    MaterialModule,
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    NgxSmartModalModule.forRoot(),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
