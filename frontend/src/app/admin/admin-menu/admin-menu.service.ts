import { PostDTO } from './../../models/post.dto';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminMenuService {

  constructor(private readonly client: HttpClient) {}

  public getFlaggedPosts(): Observable<PostDTO[]> {
    return this.client.get<PostDTO[]>(
      `http://localhost:3000/posts/flaggedPosts`
    );
  }

  public getSearchedUsers(value: string): Observable<PostDTO[]> {
    return this.client.get<PostDTO[]>(
      `http://localhost:3000/users/name?name=${value}`
    );
  }
}
