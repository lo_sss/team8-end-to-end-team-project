import { StorageService } from './../../public-part/log-in/storage.service';
import { HttpHeaders } from '@angular/common/http';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { PostDTO } from './../../models/post.dto';
import { HttpClient } from '@angular/common/http';
import { AdminMenuService } from './admin-menu.service';
import { ActivatedRoute } from '@angular/router';

import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.css']
})
export class AdminMenuComponent implements OnInit {
  posts: any
  hasError = false;
  message = "";
  searchText: ""
  users: any
  selectedUser: any
  fullImagePath = '/assets/images/MrSmiley.jpg'

  constructor(
    private readonly route: ActivatedRoute,
    private readonly adminMenuService: AdminMenuService,
    private readonly client: HttpClient,
    public ngxSmartModalService: NgxSmartModalService,
    private readonly storage: StorageService,
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(() => {
      this.adminMenuService.getFlaggedPosts().subscribe({
        next: data => {
          this.posts = data;
          this.hasError = false;
          if (!data.length) {
            this.hasError = true;
            this.message = "No Results";
          }
        },
        error: e => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = "No flagged posts";
          }
        }
      });
    });
  }

  public approvePost(post) {
    const newPosts = this.posts.filter(x => x.id !== post.id)
    this.posts = newPosts

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem("token")}`,
    });
    this.client
      .delete<PostDTO>(
        `http://localhost:3000/posts/${post.id}/flag`,
        {
          headers
        },
      )
      .subscribe({
        next: data => console.log('Success' + data),
        error: err => this.message = "Can not approve this topic",
      });
  }

  public deletePost(post) {
    const newPosts = this.posts.filter(x => x.id !== post.id)
    this.posts = newPosts

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem("token")}`,
    });
    this.client
      .delete<PostDTO>(
        `http://localhost:3000/posts/${post.id}`,
        {
          headers
        },
      )
      .subscribe({
        next: data => console.log('Success' + data),
        error: err => this.message = "Can not delete this topic",
      });

      post.name = "This topic was deleted!"
      post.content = "This topic content was deleted!"


  }

  public getUsers(){
  this.adminMenuService.getSearchedUsers(this.searchText).subscribe({
    next: data => {
      this.users = data;
      this.hasError = false;
      if (!data.length) {
        this.hasError = true;
        this.message = "No Results";
      }
    },
    error: e => {
      if (e.error.statusCode === 404) {
        this.hasError = true;
        this.message = "No results";
      }
    }
  });
}

public selectUser(user) {
  this.selectedUser = user
}

public openModal(){
  this.ngxSmartModalService.getModal('banned-user-modal').open()
}


public banUser(user, period) {
  if(!this.selectedUser){
    return
  }
  const headers = new HttpHeaders({
    Authorization: `Bearer ${this.storage.getItem("token")}`,
  });
  this.client
    .put<PostDTO>(
      `http://localhost:3000/users/${user.id}/ban/${period}`,
      {},
      {
        headers
      },
    )
    .subscribe({
      next: data => console.log('Success' + data),
      error: err => this.message = "Can not ban user",
    });
    this.openModal()
}

  }
