import { UserDTO } from './../models/user.dto';
import { CommentDTO } from './../models/comment.dto';
import { SearchResultsService } from "./search-results.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { PostDTO } from "../models/post.dto";

@Component({
  selector: "app-search-results",
  templateUrl: "./search-results.component.html",
  styleUrls: ["./search-results.component.css"]
})
export class SearchResultsComponent implements OnInit {
  posts: PostDTO[];
  hasError = false;
  message = "";
  comments: any
  users: any

  constructor(
    private readonly route: ActivatedRoute,
    private readonly searchService: SearchResultsService
  ) {}

  getUrl = post => {
    return `/posts/${post.id}`;
  };

  getUserUrl = user => {
    return `/users/${user.id}`;
  };

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.searchService.getPosts(params.value).subscribe({
        next: data => {
          this.posts = data;
          this.hasError = false;
          if (!data.length) {
            this.hasError = true;
            this.message = "No Results";
          }
        },
        error: e => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = "No results";
          }
        }
      });
    });

    this.route.params.subscribe(params => {
      this.searchService.getComments(params.value).subscribe({
        next: data => {
          this.comments = data;
          this.hasError = false;
          if (!data.length) {
            this.hasError = true;
            this.message = "No Results";
          }
        },
        error: e => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = "No results";
          }
        }
      });
    });

    this.route.params.subscribe(params => {
      this.searchService.getUsers(params.value).subscribe({
        next: data => {
          this.users = data;
          this.hasError = false;
          if (!data.length) {
            this.hasError = true;
            this.message = "No Results";
          }
        },
        error: e => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = "No results";
          }
        }
      });
    });
  }
}
