import { UserDTO } from './../models/user.dto';
import { CommentDTO } from './../models/comment.dto';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { PostDTO } from "../models/post.dto";

@Injectable({
  providedIn: "root"
})
export class SearchResultsService {
  constructor(private readonly client: HttpClient) {}

  public getPosts(value: string): Observable<PostDTO[]> {
    return this.client.get<PostDTO[]>(
      `http://localhost:3000/posts?name=${value}`
    );
  }

  public getComments(value: string): Observable<CommentDTO[]> {
    return this.client.get<CommentDTO[]>(
      `http://localhost:3000/posts/0/comments/content?content=${value}`
    );
  }

  public getUsers(value: string): Observable<UserDTO[]> {
    return this.client.get<UserDTO[]>(
      `http://localhost:3000/users/name?name=${value}`
    );
  }
}
