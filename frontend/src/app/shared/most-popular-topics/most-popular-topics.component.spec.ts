import { MostPopularTopicsComponent } from "./most-popular-topics.component";
import { ComponentFixture, TestBed, async } from "@angular/core/testing";
import { RouterModule } from "@angular/router";
import { MostPopularTopicsService } from "./most-popular-topics.service";
import { APP_BASE_HREF } from "@angular/common";

describe("MostPopularTopicsComponent", () => {
  let component: MostPopularTopicsComponent;
  let html: any;
  let fixture: ComponentFixture<MostPopularTopicsComponent>;
  let mockMostPopularService = {
    getPosts: () => [],
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MostPopularTopicsComponent],
      imports: [RouterModule.forRoot([])],
      providers: [
        MostPopularTopicsService,
        { provide: APP_BASE_HREF, useValue: "/" },
      ],
    })
      .overrideProvider(MostPopularTopicsService, {
        useValue: mockMostPopularService,
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostPopularTopicsComponent);
    component = fixture.componentInstance;
    html = fixture.nativeElement;
    // fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it(`must work!`, () => {
    expect(1).toBeTruthy();
  });

  // it(`should render links correctly`, () => {
  //   expect(component.posts).toEqual([]);
  // });

  // it(`should receive the correct posts`, () => {
  //   const mockValue = [
  //     { id: "1", name: "Name1", content: "Content1", category: "Fun" },
  //     { id: "2", name: "Name2", content: "Content2", category: "Fun" },
  //   ];
  //   jest.spyOn(mockMostPopularService, "getPosts").mockReturnValue(mockValue);

  //   component.ngOnInit();

  //   expect(component.posts).toEqual([]);
  // });
});
