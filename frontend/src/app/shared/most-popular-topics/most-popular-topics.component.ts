import { MostPopularTopicsService } from "./most-popular-topics.service";
import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-most-popular-topics",
  templateUrl: "./most-popular-topics.component.html",
  styleUrls: ["./most-popular-topics.component.css"]
})
export class MostPopularTopicsComponent implements OnInit {
  posts: any;
  hasError = false;
  message = "";

  constructor(
    private readonly route: ActivatedRoute,
    private readonly mostPopularService: MostPopularTopicsService
  ) {}

  getUrl = post => {
    return `/posts/${post.id}`;
  };

  ngOnInit(): void {
    this.route.params.subscribe(() => {
      this.mostPopularService.getPosts().subscribe({
        next: data => {
          this.posts = data;
          this.hasError = false;
          if (!data.length) {
            this.hasError = true;
            this.message = "No Results";
          }
        },
        error: e => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = "No results";
          }
        }
      });
    });
  }
}
