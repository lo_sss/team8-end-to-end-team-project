import { PostDTO } from "../../models/post.dto";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class MostPopularTopicsService {
  constructor(private readonly client: HttpClient) {}

  public getPosts(): Observable<PostDTO[]> {
    return this.client.get<PostDTO[]>(
      `http://localhost:3000/posts/mostpopular`
    );
  }
}
