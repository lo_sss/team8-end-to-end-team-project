import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mr-smiley',
  templateUrl: './mr-smiley.component.html',
  styleUrls: ['./mr-smiley.component.css'],
})
export class MrSmileyComponent implements OnInit {
  fullImagePath: string;
  randomFact = ""
  constructor() {
    this.fullImagePath = '/assets/images/MrSmiley.jpg'
  }

  ngOnInit(): void {
    const myArray = [
      "There are more possible iterations of a game of chess than there are atoms in the known universe.",
      "It can take a photon 40,000 years to travel from the core of the sun to the surface, but only 8 minutes to travel the rest of the way to earth.",
      "It would take 1,200,000 mosquitoes, each sucking once, to completely drain the average human of blood.",
      "Basically anything that melts can be made into glass. You just have to cool off a molten material before its molecules have time to realign into what they were before being melted.",
      "A small percentage of the static you see on dead tv stations is left over radiation from the Big Bang. You're seeing residual effects of the Universe's creation.",
      "Written language was invented independently by the Egyptians, Sumerians, Chinese, and Mayans.",
      "If you were to remove all of the empty space from the atoms that make up every human on earth, the entire world population could fit into an apple.",
      "Honey does not spoil. You could feasibly eat 3000 year old honey.",
      "If you somehow found a way to extract all of the gold from the bubbling core of our lovely little planet, you would be able to cover all of the land in a layer of gold up to your knees.",
      "The Spanish national anthem has no words.",
      "Dead people can get goose bumps."
    ];
    
    this.randomFact = myArray[Math.floor(Math.random()*myArray.length)];
  }
}
