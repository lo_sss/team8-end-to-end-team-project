import { PostDTO } from './../../models/post.dto';
import { ActivatedRoute } from '@angular/router';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  SimpleChanges,
  Input,
} from '@angular/core';
import { FavouriteTopicsService } from './favourite-topics.service';
import { UserDTO } from '../../models/user.dto';
import { Subject } from 'rxjs';
import { SharedService } from 'src/app/public-part/shared.service';

@Component({
  selector: 'app-favourite-topics',
  templateUrl: './favourite-topics.component.html',
  styleUrls: ['./favourite-topics.component.css'],
})
export class FavouriteTopicsComponent implements OnInit {
  @Input() updateFavorites: Subject<any> = new Subject<any>();
  @Input()
  favoriteData: any;
  posts: PostDTO[];
  hasError = false;
  message = '';
  user: UserDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly favouriteService: FavouriteTopicsService,
    private readonly sharedService: SharedService,
  ) {}

  getUrl = post => {
    return `/posts/${post.id}`;
  };

  ngOnInit(): void {
    this.updateFavorites.subscribe(response => {
      if (response) {
        this.posts.push(response);
        // Or do whatever operations you need.
      }
    });
    this.route.params.subscribe(() => {
      const user = this.sharedService.getUser()
      this.favouriteService.getFav(user.id).subscribe({
        next: data => {
          this.posts = data as any;
          this.hasError = false;
          if (!data.length) {
            this.hasError = true;
            this.message = 'No Results';
          }
        },
        error: e => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = 'No results';
          }
        },
      });
    });
  }
}
