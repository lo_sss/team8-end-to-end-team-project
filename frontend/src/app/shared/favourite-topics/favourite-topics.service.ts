import { StorageService } from "./../../public-part/log-in/storage.service";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class FavouriteTopicsService {
  constructor(
    private readonly client: HttpClient,
    private readonly storage: StorageService
  ) {}

  public getFav(userId): Observable<any[]> {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem("token")}`,
    });

    return this.client.get<any[]>(
      `http://localhost:3000/users/${userId}/favposts`,
      { headers }
    );
  }
}
