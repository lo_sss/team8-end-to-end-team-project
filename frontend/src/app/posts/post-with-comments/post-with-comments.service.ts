import { StorageService } from "./../../public-part/log-in/storage.service";
import { CommentDTO } from "./../../models/comment.dto";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class PostWithCommentsService {
  constructor(
    private readonly client: HttpClient,
    private readonly storage: StorageService
  ) {}

  public getPost(id: string): Observable<any> {
    return this.client.get<any>(`http://localhost:3000/posts/${id}`);
  }

  public createComment(comment: CommentDTO): Observable<CommentDTO> {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem("token")}`,
    });

    return this.client.post<CommentDTO>(
      `http://localhost:3000/posts/${comment.postId}/comments`,
      comment,
      { headers }
    );
  }
}
