import { SharedService } from './../../public-part/shared.service';
import { UserDTO } from './../../models/user.dto';
import { PostDTO } from './../../models/post.dto';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommentDTO } from './../../models/comment.dto';
import { PostWithCommentsService } from './post-with-comments.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Subject } from 'rxjs';
import { StorageService } from '../../public-part/log-in/storage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-with-comments',
  templateUrl: './post-with-comments.component.html',
  styleUrls: ['./post-with-comments.component.css'],
})
export class PostWithCommentsComponent implements OnInit {
  post: any;
  updateFavorites: Subject<any> = new Subject<any>();
  hasError = false;
  message = '';
  comments: CommentDTO[] = [];
  postId: string;
  modalUsersPostLikes: [];
  favoriteData: {};
  modalUsersCommentsLikes: [];
  isUserLogged: boolean;
  private subscription: Subscription;
  infoMessage = '';
  fullImagePath = '/assets/images/MrSmiley.jpg';

  constructor(
    private readonly route: ActivatedRoute,
    private readonly postWithCommentsService: PostWithCommentsService,
    private readonly client: HttpClient,
    private readonly storage: StorageService,
    public ngxSmartModalService: NgxSmartModalService,
    private readonly sharedService: SharedService
  ) {}

  ngOnInit(): void {
    this.getPosts();

    this.subscription = this.sharedService.isUserLoggedIn$.subscribe(
      (isUserLogged) => (this.isUserLogged = isUserLogged)
    );
  }

  isOwnerofPost() {
    const user = this.sharedService.getUser();
    if (user.id === this.post?.user.id) {
      return true;
    } else {
      return false;
    }
  }

  isOwnerofComment(comment) {
    const user = this.sharedService.getUser();
    if (user.id === comment.user.id) {
      return true;
    } else {
      return false;
    }
  }

  isUserLoggedAdmin() {
    return this.isUserLogged && this.sharedService.isUserAdmin();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public openUserModal() {
    this.infoMessage = 'You already liked this comment';
    this.ngxSmartModalService.getModal('user-modal').open();
  }

  public openUserPostModal() {
    this.infoMessage = 'You already liked this topic';
    this.ngxSmartModalService.getModal('user-modal').open();
  }

  public openFavouriteModal() {
    this.infoMessage = 'You already added this topic to favourites';
    this.ngxSmartModalService.getModal('user-modal').open();
  }

  public openFlagModal() {
    this.infoMessage = 'You flagged this topic';
    this.ngxSmartModalService.getModal('user-modal').open();
  }

  onlyVisibleMode() {
    const user = this.sharedService.getUser() || {};
    if (this.isUserLoggedAdmin() || !this.post) {
      return false;
    }
    
    return !this.isUserLogged || user.isBanned || this.post.isLocked;
  }

  updateFavoritesInChild(newFavorite) {
    this.updateFavorites.next(newFavorite);
  }

  public openPostModal(post: any) {
    this.modalUsersPostLikes = post.usersPostLikes;
    this.ngxSmartModalService.getModal('post-likes-modal').open();
  }

  public openCommentModal(comment: any) {
    this.modalUsersCommentsLikes = comment.usersLikes;
    this.ngxSmartModalService.getModal('comment-likes-modal').open();
  }

  public createComment(comment: CommentDTO) {
    this.postWithCommentsService.createComment(comment).subscribe({
      next: (data) => {
        this.post.comments.push({ ...data, usersLikes: [] });
      },
      error: (err) => console.log('Error' + err),
    });
  }

  public getPosts() {
    this.route.params.subscribe((params) => {
      this.postId = params.id;
      this.postWithCommentsService.getPost(params.id).subscribe({
        next: (data) => {
          this.post = data;
          this.hasError = false;
        },
        error: (e) => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = 'No content to show';
          }
        },
      });
    });
  }

  public editComment(comment: CommentDTO) {
    comment.inEditMode = true;
  }

  public saveComment(comment: CommentDTO) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });

    comment.inEditMode = false;

    this.client
      .put<CommentDTO>(
        `http://localhost:3000/posts/${this.post.id}/comments/${comment.id}`,
        comment,
        { headers }
      )
      .subscribe({
        next: (data) => {
          console.log('Success' + data)
        } ,
        error: (err) => {
          console.log('Error' + err);
          this.hasError = true;
          this.message = 'Can not update comment';
        },
      });
  }

  public editPost(post: CommentDTO) {
    post.inEditMode = true;
  }

  public savePost(post: PostDTO) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });

    post.inEditMode = false;

    this.client
      .put<PostDTO>(`http://localhost:3000/posts/${this.post.id}`, post, {
        headers,
      })
      .subscribe({
        next: (data) => console.log('Success' + data),
        error: (err) => {
          console.log('Error' + err);
          this.hasError = true;
          this.message = 'Can not update topic';
        },
      });
  }

  public deleteComment(comment: CommentDTO) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });

    this.client
      .delete<CommentDTO>(
        `http://localhost:3000/posts/${this.post.id}/comments/${comment.id}`,
        { headers }
      )
      .subscribe({
        next: (data) => console.log('Success' + data),
        error: (err) => {
          console.log('Error' + err);
          this.hasError = true;
          this.message = 'Can not delete comment';
        },
      });

    comment.content = 'This comment was deleted!';
  }

  public deletePost(post: PostDTO) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });

    this.client
      .delete<PostDTO>(`http://localhost:3000/posts/${this.post.id}`, {
        headers,
      })
      .subscribe({
        next: (data) => console.log('Success' + data),
        error: (err) => {
          console.log('Error' + err);
          this.hasError = true;
          this.message = 'Can not delete topic ';
        },
      });

    post.name = 'This topic was deleted!';
    post.content = 'This topic content was deleted!';
  }

  public likeComment(comment) {
    const user = this.sharedService.getUser() || {};
    const likedUser = comment.usersLikes.find((x) => x.id === user.id);

    if (likedUser) {
      this.openUserModal();
    } else {
      const headers = new HttpHeaders({
        Authorization: `Bearer ${this.storage.getItem('token')}`,
      });

      this.client
        .post<UserDTO[]>(
          `http://localhost:3000/posts/${this.post.id}/comments/${comment.id}/likes`,
          {},
          { headers }
        )
        .subscribe({
          next: (data) => {
            comment.usersLikes.push(data);
          },
          error: (err) => {
            console.log('Error' + err);
            this.hasError = true;
            this.message = 'Can not like comment';
          },
        });
    }
  }

  public likePost(post) {
    const user = this.sharedService.getUser() || {};
    const likedUser = post.usersPostLikes.find((x) => x.id === user.id);

    if (likedUser) {
      this.openUserPostModal();
    } else {
      const headers = new HttpHeaders({
        Authorization: `Bearer ${this.storage.getItem('token')}`,
      });
      this.client
        .post<UserDTO[]>(
          `http://localhost:3000/posts/${this.post.id}/likes`,
          {},
          { headers }
        )
        .subscribe({
          next: (data) => {
            console.log(data);
            post.usersPostLikes.push(data);
          },
          error: (err) => {
            if(err.error.error === "Post already liked by this user"){
              
              this.infoMessage=err.error.error
              this.openUserPostModal()
            } else {
              this.hasError = true;
              this.message = 'Can not favourite topic';
            }
          },
        });
    }
  }

  public favouritePost(post) {
    const user = this.sharedService.getUser() || {};
    const favPost = user.favouritePosts?.find((x) => x.id === post.id);

    if (favPost) {
      this.openFavouriteModal();
    } else {
      const headers = new HttpHeaders({
        Authorization: `Bearer ${this.storage.getItem('token')}`,
      });

      this.client
        .put<UserDTO[]>(
          `http://localhost:3000/users/1/favouritePosts/${post.id}`,
          {},
          { headers }
        )
        .subscribe({
          next: (data) => {
            console.log('Favorited Successfuly');
            this.updateFavoritesInChild(data);
          },
          error: (err) => {
            if(err.error.error === "You already added this post to favourite"){
              
              this.infoMessage=err.error.error
              this.openFavouriteModal()
            } else {
              this.hasError = true;
              this.message = 'Can not favourite topic';
            }
          },
        });
    }
  }

  public flagPost(post) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });
    this.client
      .put<UserDTO[]>(
        `http://localhost:3000/posts/${post.id}/flag`,
        {},
        { headers }
      )
      .subscribe({
        next: (data) => {
          console.log('Flagged Successfuly');
        },
        error: (err) => {
          console.log('Error' + err);
          this.hasError = true;
          this.message = 'Can not flag topic';
        },
      });

    this.openFlagModal();
  }

  public lockPost(post) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });
    this.client
      .put<UserDTO[]>(
        `http://localhost:3000/posts/${post.id}/lock`,
        {},
        { headers }
      )
      .subscribe({
        next: (data) => {
          this.post.isLocked = true
          console.log('Locked Successfuly');
        },
        error: (err) => {
          console.log('Error' + err);
          this.hasError = true;
          this.message = 'Can not lock topic';
        },
      });
  }

  public unlockPost(post: PostDTO) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });
    this.client
      .delete<PostDTO>(`http://localhost:3000/posts/${post.id}/lock`, {
        headers,
      })
      .subscribe({
        next: (data) => console.log('Post is unlock' + data),
        error: (err) => {
          console.log('Error' + err);
          this.hasError = true;
          this.message = 'Can not unloch topic';
        },
      });
  }
}
