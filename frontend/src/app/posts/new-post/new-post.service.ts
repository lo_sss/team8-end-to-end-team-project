import { PostDTO } from "./../../models/post.dto";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageService } from "../../public-part/log-in/storage.service";

@Injectable({
  providedIn: "root",
})
export class NewPostService {
  constructor(
    private readonly client: HttpClient,
    private readonly storage: StorageService
  ) {}

  public createPost(post: PostDTO): any {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem("token")}`,
    });

    return this.client.post<PostDTO>(`http://localhost:3000/posts`, post, {
      headers,
    });
  }
}
