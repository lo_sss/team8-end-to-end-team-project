import { NgxSmartModalService } from 'ngx-smart-modal';
import { NewPostService } from './new-post.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PostDTO } from './../../models/post.dto';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent {

  @Input()
  public post: PostDTO = {};
  public content = ""
  public user = "1"
  public name = ""
  public category = ""
  hasError = false
  message = ""
  infoMessage = 'You must fill topic title, topic content and choose category.';
  fullImagePath = '/assets/images/MrSmiley.jpg'
  

  getUrl = post => {
    return `/posts/${post.id}`;
  };

  constructor(private readonly route: ActivatedRoute, 
  private readonly newPostService: NewPostService, 
  private readonly router: Router,
  public ngxSmartModalService: NgxSmartModalService) {}

  onPostSuccess(data){
    this.router.navigate(['/posts/', data.id])
  }

  public openModal(){
    this.ngxSmartModalService.getModal('new-post-modal').open()
  }

  createPost(): void {
    const post = {
      name: this.name,
      user: this.user,
      content: this.content,
      category: this.category
    }

    if(this.name === "" || this.content === "" || this.category === "") {
      this.openModal()
    }
    else{
     this.newPostService.createPost(post).subscribe({
      next: data => {
        this.onPostSuccess(data)
        this.hasError = false
      } ,
      error: err => {
      this.hasError = true
      this.message = "Can not create new topic"}
    });

    this.name = ""
    this.content = ""
    this.category = ""
  }
}
}
