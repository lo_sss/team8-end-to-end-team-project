import { StorageService } from "./../../public-part/log-in/storage.service";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CommentDTO } from "./../../models/comment.dto";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class NewCommentService {
  constructor(
    private readonly client: HttpClient,
    private readonly storage: StorageService
  ) {}

  public createComment(comment: CommentDTO): any {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem("token")}`,
    });

    return this.client
      .post<CommentDTO>(
        `http://localhost:3000/posts/${comment.postId}/comments`,
        comment,
        { headers }
      )
      .subscribe({
        next: (data) => console.log("Success" + data),
        error: (err) => console.log("Error" + err),
      });
  }
}
