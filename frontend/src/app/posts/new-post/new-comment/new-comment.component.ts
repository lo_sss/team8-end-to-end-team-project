import { ActivatedRoute } from '@angular/router';
import { CommentDTO } from './../../../models/comment.dto';
import { NewCommentService } from './../new-comment.service';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-new-comment',
  templateUrl: './new-comment.component.html',
  styleUrls: ['./new-comment.component.css'],
})
export class NewCommentComponent {
  @Input()
  public comment: CommentDTO = {};
  @Input()
  public postId: string
  @Output()
  public commentCreated: EventEmitter<CommentDTO> = new EventEmitter<CommentDTO>();
  public content = ""
  public singlePost = ""
  public user = "1"

  constructor(private readonly route: ActivatedRoute, 
  private readonly newCommentService: NewCommentService) {}

  createComment(): void {
    const comment = {
      postId: this.postId,
      userId: this.user,
      content: this.content
    }

    this.commentCreated.emit(comment);
    this.content = ""
  }
}
