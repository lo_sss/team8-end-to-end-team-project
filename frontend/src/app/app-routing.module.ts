import { SearchResultsComponent } from "./search-results/search-results.component";
import { AdminComponent } from "./admin/admin.component";
import { ProfileComponent } from "./profile/profile.component";
import { PostWithCommentsComponent } from "./posts/post-with-comments/post-with-comments.component";
import { CategoryComponent } from "./home/category/category.component";
import { NewPostComponent } from "./posts/new-post/new-post.component";
import { LogInComponent } from "./public-part/log-in/log-in.component";
import { SignInComponent } from "./public-part/sign-in/sign-in.component";
import { HomepagePublicComponent } from "./public-part/homepage-public/homepage-public.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";

const routes: Routes = [
  { path: "", component: HomepagePublicComponent },
  { path: "registration", component: SignInComponent },
  { path: "login", component: LogInComponent },
  { path: "home", component: HomeComponent },
  { path: "results/:value", component: SearchResultsComponent },
  { path: "posts/new-post", component: NewPostComponent },
  { path: "posts/:id", component: PostWithCommentsComponent },
  { path: "posts/list/category", component: CategoryComponent },
  { path: "users/:id", component: ProfileComponent },
  { path: "admin", component: AdminComponent },
  { path: "posts/categories/:category", component: CategoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
