import { CommentDTO } from "./comment.dto";
export class PostDTO {
    id?: string
    userId?: string
    name?: string
    content?: string
    comments?: CommentDTO[]
    date?: string
    category?: string
    inEditMode?: boolean
}
