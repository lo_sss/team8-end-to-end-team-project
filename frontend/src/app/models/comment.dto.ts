export class CommentDTO {
    id?: string
    postId?: string
    userId?: string
    content?: string
    date?: string
    inEditMode?: boolean
}