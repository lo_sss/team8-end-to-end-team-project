import { PostDTO } from "./post.dto";

export class UserDTO {
  id: string;
  favouritePosts: Promise<PostDTO[]>;
}
