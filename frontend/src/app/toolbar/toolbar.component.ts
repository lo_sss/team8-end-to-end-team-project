import { SharedService } from "./../public-part/shared.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";

@Component({
  selector: "app-toolbar",
  templateUrl: "./toolbar.component.html",
  styleUrls: ["./toolbar.component.css"],
})
export class ToolbarComponent implements OnInit {
  searchText: "";
  isUserLogged: boolean;
  private subscription: Subscription;

  constructor(
    private readonly router: Router,
    private readonly sharedService: SharedService
  ) {}

  public search(): void {
    this.router.navigate(["/results", this.searchText]);
    this.searchText = ""
  }

  

  isUserLoggedAdmin(){
    return this.isUserLogged 
    && this.sharedService.isUserAdmin()
  }

  isUserBanned(){
    const user = this.sharedService.getUser()
    if(user.isBanned) {
      return true
    }
    return false
  }

  public onLogout(){
    this.sharedService.isUserLoggedIn$.next(false)
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    this.router.navigate(["/"]);
  }

  public getUserUrl(){
    const user = this.sharedService.getUser() || ""
    return `users/${user.id}`
  }

  ngOnInit(): void {
    this.subscription = this.sharedService.isUserLoggedIn$.subscribe(
      (isUserLogged) => (this.isUserLogged = isUserLogged)
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
