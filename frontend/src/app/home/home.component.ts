import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PostDTO } from "../models/post.dto";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit {
  posts: PostDTO[];
  hasError = false;
  message = "";

  constructor(private readonly router: Router) {}

  navigateCategory(category): void {
    if (
      category === "Animals" ||
      category === "Fun" ||
      category === "Games" ||
      category === "History" ||
      category === "Math" ||
      category === "Music" ||
      category === "Other" ||
      category === "Plants" ||
      category === "Telerik Academy"
    ) {
      this.router.navigate([`posts/categories`, category]);
    }
  }

  ngOnInit(): void {
    //
  }
}
