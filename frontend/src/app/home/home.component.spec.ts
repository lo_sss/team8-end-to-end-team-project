import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HomeComponent } from "./home.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { RouterTestingModule } from "@angular/router/testing";
import { MaterialModule } from "../material/material.module";
import { Router } from "@angular/router";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

describe("HomeComponent", () => {
  let fixture: ComponentFixture<HomeComponent>;
  let realRouter: any;
  let component: HomeComponent;

  beforeEach(async(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        BrowserModule,
        MaterialModule,
        RouterTestingModule.withRoutes([
          { path: "", redirectTo: "home", pathMatch: "full" },
          { path: `posts/categories:Category`, component: HomeComponent },
        ]),
      ],
      declarations: [HomeComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
        realRouter = TestBed.get(Router);
      });
  }));

  it("should be defined", () => {
    expect(component).toBeDefined();
  });

  it("should navigate correctly when clicked", () => {
    jest.spyOn(realRouter, "navigate");

    const category = "Fun";
    component.navigateCategory(category);

    expect(realRouter.navigate).toHaveBeenCalledWith([
      `posts/categories`,
      "Fun",
    ]);
  });

  it("should not navigate when clicked around the category", () => {
    jest.spyOn(realRouter, "navigate");

    const category = "";
    component.navigateCategory(category);

    expect(realRouter.navigate).toHaveBeenCalledTimes(0);
  });
});
