import { Component, OnInit } from "@angular/core";
import { PostDTO } from "../../models/post.dto";
import { UserDTO } from "../../models/user.dto";
import { ActivatedRoute } from "@angular/router";
import { CategoryService } from "./category.service";

@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.css"]
})
export class CategoryComponent implements OnInit {
  posts: PostDTO[];
  hasError = false;
  message = "";
  user: UserDTO;
  category = ""

  constructor(
    private readonly route: ActivatedRoute,
    private readonly categoryService: CategoryService
  ) {}

  getUrl = post => {
    return `/posts/${post.id}`;
  };

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.category = params.category
      this.categoryService.getCategoryPosts(params.category).subscribe({
        next: data => {
          this.posts = data;
          this.hasError = false;
          if (!data.length) {
            this.hasError = true;
            this.message = "No Topics from that category";
          }
        },
        error: e => {
          if (e.error.statusCode === 404) {
            this.hasError = true;
            this.message = "No topics from that category";
          }
        }
      });
    });
  }
}
