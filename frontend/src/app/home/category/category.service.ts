import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { PostDTO } from "../../models/post.dto";

@Injectable({
  providedIn: "root"
})
export class CategoryService {
  constructor(private readonly client: HttpClient) {}

  public getCategoryPosts(category: string): Observable<PostDTO[]> {
    return this.client.get<PostDTO[]>(
      `http://localhost:3000/posts/categories/${category}`
    );
  }
}
