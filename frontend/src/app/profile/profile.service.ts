import { StorageService } from './../public-part/log-in/storage.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  constructor(
    private readonly client: HttpClient,
    private readonly storage: StorageService,
  ) {}

  public getProfile(id: string): any {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });
    return this.client.get<any>(`http://localhost:3000/users/profile/${id}`, {
      headers,
    });
  }

  public addToFriends(id: string): any {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });
    return this.client.put<any>(
      `http://localhost:3000/users/friends/${id}`,
      {},
      {
        headers,
      },
    );
  }

  public removeFromFriends(id: string): any {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });
    return this.client.delete<any>(
      `http://localhost:3000/users/friends/${id}`,
      {
        headers,
      },
    );
  }

  public addAdmin(id: string): any {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.storage.getItem('token')}`,
    });
    return this.client.put<any>(
      `http://localhost:3000/users/${id}/roles`,
      {
        roles: [
          {
            id: 1,
            name: "Basic"
          },
          {
            id: 2,
            name: 'Admin',
          },
        ],
      },
      {
        headers,
      },
    );
  }
}
