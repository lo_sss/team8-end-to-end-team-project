import { Subscription } from 'rxjs';
import { SharedService } from './../public-part/shared.service';
import { StorageService } from './../public-part/log-in/storage.service';
import { Component, OnInit } from '@angular/core';
import { ProfileService } from './profile.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  clickedUserId: string;
  profile: any;
  allItems: any;
  userChronology: any;
  isUserLogged: boolean;
  private subscription: Subscription;
  hasError = false
  message = ""

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly profileService: ProfileService,
    private readonly storage: StorageService,
    private readonly sharedService: SharedService,
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.clickedUserId = params.id;
      this.profileService.getProfile(this.clickedUserId).subscribe({
        next: data => {
          this.profile = data;
          this.userChronology = this.getChronology(data);
        },
        error: e => {
        this.hasError = true
        this.message = "No activity to show"
        },
      });
    });

    this.subscription = this.sharedService.isUserLoggedIn$.subscribe(
      isUserLogged => (this.isUserLogged = isUserLogged),
    );
  }

  public navigateToProfile(profile) {
    this.router.navigate([`users/`, profile.id]);
  }

  isUserLoggedAdmin() {
    return this.isUserLogged && this.sharedService.isUserAdmin();
  }

  isProfileAlreadyAdmin() {
    return (
      this.profile && this.profile.roles.find(role => role.name === 'Admin')
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onlyVisibleMode() {
    const user = this.sharedService.getUser() || {};
    return !user || user.isBanned;
  }

  isSameUser() {
    const user = this.sharedService.getUser() || {};
    if (this.profile && user.id === this.profile.id) {
      return false;
    }
    return true;
  }

  isUserFriend() {
    const loggedUser = this.sharedService.getUser();
    return (
      this.profile &&
      this.profile.friends.find(user => loggedUser && user.id === loggedUser.id)
    );
  }

  public getChronology(data) {
    const newPosts = (data.posts || []).map(post => {
      return {
        id: post.id,
        type: 'post',
        user: this.clickedUserId,
        userName: data.name,
        content: post.name,
        date: Date.parse(post.postedOn),
        dateDisplay: post.postedOn,
        isLiked: false,
      };
    });

    const newComments = (data.comments || []).map(comment => {
      return {
        id: comment.id,
        type: 'comment',
        user: this.clickedUserId,
        userName: data.name,
        content: comment.content,
        date: Date.parse(comment.commentOn),
        dateDisplay: comment.commentOn,
        isLiked: false,
      };
    });

    const newLikedPosts = data.likedPosts.map(post => {
      return {
        user: post.userId,
        type: 'post',
        userName: post.username,
        content: post.content,
        date: Date.parse(post.postedOn),
        dateDisplay: post.postedOn,
        isLiked: true,
      };
    });

    const newLikedComments = data.likedComments.map(comment => {
      return {
        user: comment.userId,
        type: 'comment',
        userName: comment.username,
        content: comment.content,
        date: Date.parse(comment.commentOn),
        dateDisplay: comment.commentOn,
        isLiked: true,
      };
    });

    this.allItems = [
      ...newPosts,
      ...newComments,
      ...newLikedPosts,
      ...newLikedComments,
    ];
    this.allItems.sort((a, b) => b.date - a.date);
    return this.allItems;
  }

  public addFriend() {
    this.route.params.subscribe(params => {
      this.clickedUserId = params.id;
      this.profileService.addToFriends(this.clickedUserId).subscribe({
        next: data => {
          const loggedUser = this.sharedService.getUser();
          this.profile.friends.push(loggedUser);
        },
        error: e => {
        this.hasError = true
        this.message = "Can not add friend"
        },
      });
    });
  }

  public removeFromFriends() {
    this.route.params.subscribe(params => {
      this.clickedUserId = params.id;
      this.profileService.removeFromFriends(this.clickedUserId).subscribe({
        next: data => {
          const loggedUser = this.sharedService.getUser();
          this.profile.friends = this.profile.friends.filter(
            user => user.id !== loggedUser.id,
          );
        },
        error: e => {
          this.hasError = true
        this.message = "Can not remove from friends"
        },
      });
    });
  }

  public updateUserRoles() {
    this.route.params.subscribe(params => {
      this.clickedUserId = params.id;
      this.profileService.addAdmin(this.clickedUserId).subscribe({
        next: data => {
          console.log("Success");
        },
        error: e => {
        this.hasError = true
        this.message = "Can not make admin"
        },
      });
    });
  }
}
