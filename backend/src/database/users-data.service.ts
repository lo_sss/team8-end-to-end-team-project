import { User } from './../data/entities/user.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UsersDataService {
  private _idIncrementor = 0;
  public users: User[] = [];

  public get id() {
    return this._idIncrementor++;
  }
}