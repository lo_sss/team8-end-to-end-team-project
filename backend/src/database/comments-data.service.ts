import { Injectable } from "@nestjs/common";
import { Comment } from "../data/entities/comment.entity";

@Injectable()
export class CommmentsDataService {
  private _idIncrementor = 0;
  public comments: Comment[] = [];

  public get id() {
    return this._idIncrementor++;
  }
}
