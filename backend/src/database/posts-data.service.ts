import { SinglePost } from './../data/entities/singlePost.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class PostsDataService {
  private _idIncrementor = 0;
  public posts: SinglePost[] = [];

  public get id() {
    return this._idIncrementor++;
  }
}