import { User } from './../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CustomSystemError } from './../common/exceptions/custum-system.error';
import { UsersService } from './../users/users.service';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDTO } from '../models/user/user-create-dto';
import { Repository } from 'typeorm';
import * as bcrypt from "bcrypt";
import moment = require('moment');

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];

  public constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  public async findUserByName(name: string): Promise<User> {
    return await this.userRepo.findOne({
      name,
      isDeleted: false,
    });
  }

  public async validateUserPassword(user: CreateUserDTO): Promise<boolean> {
    const userEntity: User = await this.userRepo.findOne({
      name: user.name,
    });
    return await bcrypt.compare(user.password, userEntity.password);
  }

  public async login(user: CreateUserDTO): Promise<{ token: string }> {
    const foundUser: any = await this.findUserByName(
      user.name,
    );

    if(foundUser.isBanned && moment() > moment(foundUser.bannedUntil)){
      await this.userRepo.save({ ...foundUser, isBanned: false })
    }

    if (!foundUser) {
      throw new CustomSystemError(
        'User with such username does not exist',
        400,
      );
    }
    if (!(await this.validateUserPassword(user))) {
      throw new CustomSystemError('Invalid password', 400);
    }

    const payload: any = { id: foundUser.id, name: foundUser.name };

    return {
      token: await this.jwtService.signAsync(payload),
      user: foundUser
    } as any;
  }

  public blacklistToken(token: string): void {
    this.blacklist.push(token);
  }

  public isTokenBlacklisted(token: string): boolean {
    return this.blacklist.includes(token);
  }
}
