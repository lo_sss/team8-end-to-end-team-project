/* eslint-disable @typescript-eslint/no-unused-vars */
import { Comment } from "./comment.entity";
import { User } from "./user.entity";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany
} from "typeorm";

@Entity("posts")
export class SinglePost {
  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ type: "nvarchar", nullable: false, length: 1000 })
  public name: string;

  @Column({ type: "nvarchar", nullable: false, length: 1000 })
  public content: string;

  @CreateDateColumn()
  public postedOn: Date;

  @UpdateDateColumn()
  public updatedOn: Date;

  @Column({ nullable: false })
  public category: string;

  @Column({ type: "boolean", default: false })
  public isFlagged: boolean;

  @Column({ type: "boolean", default: false })
  public isDeleted: boolean;

  @Column({ type: "boolean", default: false })
  public isLocked: boolean;

  @ManyToOne(
    type => User,
    user => user.posts
  )
  public user: Promise<User>;

  @OneToMany(
    type => Comment,
    comment => comment.singlePost
  )
  public comments: Promise<Comment[]>;

  @ManyToMany(
    type => User,
    user => user.posts
  )
  public users: Promise<User[]>;
}
