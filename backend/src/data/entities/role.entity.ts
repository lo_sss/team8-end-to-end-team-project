import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("roles")
export class Role {
  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ nullable: false })
  public name: string;
}
