import { Comment } from "./comment.entity";
import { SinglePost } from "./singlePost.entity";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  JoinTable,
  ManyToMany
} from "typeorm";
import { Role } from "./role.entity";

@Entity("users")
export class User {
  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ type: "nvarchar", unique: true, nullable: false, length: 20 })
  public name: string;

  @Column({ nullable: false })
  public password: string;

  @OneToMany(
    type => SinglePost,
    post => post.user
  )
  public posts: Promise<SinglePost[]>;

  @ManyToMany(
    type => SinglePost,
    post => post.users
  )
  @JoinTable()
  public favouritePosts: Promise<SinglePost[]>;

  @OneToMany(
    type => Comment,
    comment => comment.user
  )
  public comments: Promise<Comment[]>;

  @ManyToMany(
    type => User,
    user => user.friendsInverse
  )
  @JoinTable()
  public friends: Promise<User[]>;

  @ManyToMany(
    type => User,
    user => user.friends
  )
  public friendsInverse: Promise<User[]>;

  @ManyToMany(type => Role, { eager: true })
  @JoinTable()
  public roles: Role[];

  @Column({ type: "boolean", default: false })
  public isBanned: boolean;

  @Column({ type: "boolean", default: false })
  public isDeleted: boolean;

  @Column({ type: "date", default: false })
  public bannedDate: Date;

  @Column({ type: "date", default: false })
  public bannedUntil: Date;
}
