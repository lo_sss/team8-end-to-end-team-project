import { Column, CreateDateColumn } from "typeorm";
import { Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("likes")
export class Like {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: "integer" })
  public userId: number;

  @Column({ type: "integer", nullable: true })
  public postId: number;

  @Column({ type: "integer", nullable: true })
  public commentId: number;

  @CreateDateColumn()
  public likedOn: Date;

  @Column({ type: "boolean", default: false })
  public isDeleted: boolean;

}
