import { User } from "./user.entity";
import { SinglePost } from "./singlePost.entity";
import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne
} from "typeorm";

@Entity("comments")
export class Comment {
  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ nullable: false, length: 1000 })
  public content: string;

  @CreateDateColumn()
  public commentOn: Date;

  @Column({ type: "boolean", default: false })
  public isDeleted: boolean;

  @ManyToOne(
    type => SinglePost,
    post => post.comments
  )
  public singlePost: Promise<SinglePost>;

  @ManyToOne(
    type => User,
    user => user.comments
  )
  public user: Promise<User>;
}
