export const attachUser = (req: Request, res: Response, next: Function) => {
  (req as any).user = { id: 1, name: 'Tosho', roles: ["Basic", "Admin"], isBanned: false };
  next();
};
