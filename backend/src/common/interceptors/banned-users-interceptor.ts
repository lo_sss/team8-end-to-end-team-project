import { CallHandler, ExecutionContext, Injectable, NestInterceptor, BadRequestException } from '@nestjs/common';
import { Observable, of } from 'rxjs';

@Injectable()
export class BannedUserInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest<Request>();

    if((request as any).user && (request as any).user.isBanned && request.method !== "GET"){
      return of(new BadRequestException('User is banned!'));
    }

    return next.handle();
  }
}