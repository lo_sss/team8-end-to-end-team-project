import { CustomSystemError } from './../exceptions/custum-system.error';
import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';

@Catch(CustomSystemError)
export class PostsSystemErrorFilter implements ExceptionFilter {
  public catch(exception: CustomSystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
