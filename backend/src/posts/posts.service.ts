import { Comment } from "./../data/entities/comment.entity";
import { CustomSystemError } from "./../common/exceptions/custum-system.error";
import { SinglePost } from "./../data/entities/singlePost.entity";
import { ReturnPostDTO } from "./../models/post/return-post.dto";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { User } from "../data/entities/user.entity";
import { PostDTO } from "../models/post/post.dto";
import { CreatePostDTO } from "../models/post/create-post.dto";
import { UpdatePostDTO } from "../models/post/update-post.dto";
import { Like } from "../data/entities/like.entity";
import { LikeCreatePostDTO } from "../models/like/like-create-post-dto";
import { plainToClass } from "class-transformer";
import { ReturnUserDTO } from "src/models/user/user-return-dto";
import { Like as TypeOrmLike } from "typeorm";

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(User) private readonly usersRepo: Repository<User>,
    @InjectRepository(SinglePost)
    private readonly singlePostRepo: Repository<SinglePost>,
    @InjectRepository(Comment)
    private readonly commentRepo: Repository<Comment>,
    @InjectRepository(Like)
    private readonly likeRepo: Repository<Like>
  ) {}

  public async allPostsFromCategory(clickedCategory): Promise<SinglePost[]> {
    const categoryPosts: SinglePost[] = await this.singlePostRepo.find({
      where: { isDeleted: false, category: clickedCategory },
    });

    return categoryPosts;
  }

  public async flaggedPosts(): Promise<SinglePost[]> {
    const flaggedPosts: SinglePost[] = await this.singlePostRepo.find({
      where: { isDeleted: false, isFlagged: true },
    });
    const flaggedPostsWithUser = [];
    for (const post of flaggedPosts) {
      flaggedPostsWithUser.push({
        ...post,
        user: await post.user,
      });
    }
    return flaggedPostsWithUser;
  }

  public async allPostsByName(name): Promise<ReturnPostDTO[]> {
    if (!name) {
      throw new CustomSystemError(`No post found with name ${name}`, 404);
    }

    const filteredPosts = await this.singlePostRepo.find({
      name: TypeOrmLike(`%${name}%`),
      isDeleted: false,
    });

    const filteredPostsNew = [];
    for (const post of filteredPosts) {
      filteredPostsNew.push({
        ...post,
        user: await post.user,
        comments: await post.comments,
      });
    }
    return plainToClass(ReturnPostDTO, filteredPostsNew);
  }

  public async mostPopular(): Promise<SinglePost[]> {
    const allPosts = await this.singlePostRepo.find({
      where: { isDeleted: false },
    });

    const postWithLength = [];

    for (const post of allPosts) {
      postWithLength.push({
        postId: post.id,
        commentLength: (await post.comments).length,
      });
    }

    const sortedPostsLength = postWithLength.sort(
      (a, b) => b.commentLength - a.commentLength
    );

    const sortedPosts = [];
    for (const postWithLength of sortedPostsLength) {
      const foundPost = allPosts.find(
        (post) => post.id === postWithLength.postId
      );
      sortedPosts.push(foundPost);
    }

    return sortedPosts;
  }

  public async createPost(
    post: CreatePostDTO,
    user: ReturnUserDTO
  ): Promise<SinglePost> {
    this.validatePostParameters(post.name, post.content);

    const postEntity: SinglePost = this.singlePostRepo.create(post);
    const foundUser: User = await this.usersRepo.findOne({ id: user.id }); //username: post.username

    if (foundUser === undefined || foundUser.isDeleted || !foundUser) {
      throw new CustomSystemError("No such user found", 404);
    }

    postEntity.user = Promise.resolve(foundUser);

    const savedPost = this.singlePostRepo.save(postEntity);

    return savedPost;
  }

  public async updatePost(
    id: number,
    post: UpdatePostDTO,
    user: ReturnUserDTO
  ): Promise<ReturnPostDTO> {
    const oldPost: SinglePost = await this.findPostById(id);
    const postUser = await oldPost.user;
    const isUserAdmin = (user.roles as any).find(
      (role) => role.name === "Admin"
    );
    if (!isUserAdmin && postUser.id !== user.id) {
      throw new CustomSystemError("Unauthorized user", 401);
    }
    const oldPostLock = await oldPost.isLocked;
    if (oldPostLock && !isUserAdmin) {
      throw new CustomSystemError("This post is locked to changes", 403);
    }
    const entityToUpdate: SinglePost = { ...oldPost, ...post };

    const savedPost = this.singlePostRepo.save(entityToUpdate);
    return plainToClass(ReturnPostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  public async deletePost(id: number, user: ReturnUserDTO): Promise<PostDTO> {
    const foundPost: SinglePost = await this.findPostById(id);
    const postUser = await foundPost.user;
    const isUserAdmin = (user.roles as any).find(role => role.name === "Admin")
    if (!isUserAdmin && postUser.id !== user.id) {
      throw new CustomSystemError("Unauthorized user", 401);
    }
    const foundPostLock = await foundPost.isLocked;

    if (foundPostLock && !isUserAdmin) {
      throw new CustomSystemError("This post is locked to changes", 403);
    }

    const deletedPost = this.singlePostRepo.save({
      ...foundPost,
      isDeleted: true,
    });
    return plainToClass(PostDTO, deletedPost, {
      excludeExtraneousValues: true,
    });
  }

  public async findPostById(postId: number): Promise<SinglePost> {
    const foundPost: SinglePost = await this.singlePostRepo.findOne({
      id: postId,
    });
    if (foundPost === undefined || foundPost.isDeleted) {
      throw new CustomSystemError("No such post found", 404);
    }

    return foundPost;
  }

  public async getPostById(postId: number): Promise<SinglePost> {
    const foundPost: SinglePost = await this.singlePostRepo.findOne({
      id: postId,
    });
    if (foundPost === undefined || foundPost.isDeleted) {
      throw new CustomSystemError("No such post found", 404);
    }
    const usersPostLikes: User[] = [];
    const postLikeInstances: Like[] = await this.likeRepo.find({
      where: { postId },
    });

    for (const like of postLikeInstances) {
      const foundUser: User = await this.usersRepo.findOne({
        where: { id: like.userId },
      });
      usersPostLikes.push(foundUser);
    }

    const newPost = {
      ...foundPost,
      user: await foundPost.user,
      comments: (await foundPost.comments).filter(
        (comment) => !comment.isDeleted
      ),
      usersPostLikes,
    };

    const newComments = [];

    for (const comment of newPost.comments) {
      const commentUser = await comment.user;
      const users: User[] = [];

      const likeInstances: Like[] = await this.likeRepo.find({
        where: { commentId: comment.id },
      });
      for (const like of likeInstances) {
        const foundUser: User = await this.usersRepo.findOne({
          where: { id: like.userId },
        });
        users.push(foundUser);
      }
      newComments.push({ ...comment, user: commentUser, usersLikes: users });
    }
    return { ...newPost, comments: newComments } as any;
  }

  private validatePostParameters(postName: string, postContent: string): void {
    // if (
    //   postName === undefined ||
    //   postName === null ||
    //   postName.length < 2 ||
    //   postName.length > 200
    // ) {
    //   throw new CustomSystemError(
    //     "Title should be between 2 and 1000 characters",
    //     1000
    //   );
    // }
    if (
      postContent === undefined ||
      postContent === null ||
      postContent.length < 2 ||
      postContent.length > 1000
    ) {
      throw new CustomSystemError(
        "Post should be between 2 and 1000 characters",
        400
      );
    }
  }

  public async flagPost(
    id: number,
    user: ReturnUserDTO
  ): Promise<ReturnPostDTO> {
    const oldPost: SinglePost = await this.findPostById(id);

    const entityToUpdate: SinglePost = { ...oldPost, isFlagged: true };

    const savedPost = this.singlePostRepo.save(entityToUpdate);
    return plainToClass(ReturnPostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  public async unflagPost(id: number): Promise<ReturnPostDTO> {
    const oldPost: SinglePost = await this.findPostById(id);
    const entityToUpdate: SinglePost = { ...oldPost, isFlagged: false };

    const savedPost = this.singlePostRepo.save(entityToUpdate);
    return plainToClass(ReturnPostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  async addPostLike(id: number, user: ReturnUserDTO): Promise<Like> {
    const post: SinglePost = await this.singlePostRepo.findOne({
      where: { id, isDeleted: false },
    });
    const likeExistsCheck: Like = await this.likeRepo.findOne({
      where: { userId: user.id, postId: post.id },
    });

    if (likeExistsCheck) {
      throw new CustomSystemError("Post already liked by this user", 403);
    }

    if (post === undefined || post.isDeleted) {
      throw new CustomSystemError("No such post found", 404);
    }

    const like: LikeCreatePostDTO = {
      postId: post.id,
    };
    const likeEntity: Like = this.likeRepo.create(like);
    const newLike = { ...likeEntity, userId: user.id };
    await this.likeRepo.save(newLike);

    return plainToClass(Like, newLike, {
      excludeExtraneousValues: true,
    });
  }

  async removePostLike(id: number, user: ReturnUserDTO): Promise<Like> {
    const like: Like = await this.likeRepo.findOne({ where: { postId: id } });
    const foundPost: SinglePost = await this.singlePostRepo.findOne({
      where: { id },
    });
    if (foundPost === undefined || foundPost.isDeleted) {
      throw new CustomSystemError("No such post found", 404);
    }
    const postUser = await foundPost.user;
    const isUserAdmin = (user.roles as any).find(role => role.name === "Admin")
    if (!isUserAdmin && postUser.id !== user.id) {
      throw new CustomSystemError("Unauthorized user", 401);
    }

    const removedLike = { ...like, isDeleted: true };
    await this.likeRepo.save(removedLike);
    return plainToClass(Like, removedLike, {
      excludeExtraneousValues: true,
    });
  }

  private async findLikesByPostId(id: number): Promise<Like[]> {
    const likes = await this.likeRepo.find({
      where: { postId: id, isDeleted: false },
    });
    const foundPost: SinglePost = await this.singlePostRepo.findOne({
      where: { id },
    });
    if (foundPost === undefined || foundPost.isDeleted) {
      throw new CustomSystemError("No such post found", 404);
    }
    return likes;
  }

  public async lockPost(postId: number): Promise<ReturnPostDTO> {
    const postEntity: SinglePost = await this.singlePostRepo.findOne({
      where: { id: postId, isDeleted: false },
    });

    if (!postEntity) {
      throw new CustomSystemError("No such post found", 404);
    }

    const updatedPost: SinglePost = { ...postEntity, isLocked: true };
    const savedPost: SinglePost = await this.singlePostRepo.save(updatedPost);

    return plainToClass(ReturnPostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  public async unlockPost(postId: number): Promise<ReturnPostDTO> {
    const postEntity: SinglePost = await this.singlePostRepo.findOne({
      where: { id: postId, isDeleted: false },
    });

    if (!postEntity) {
      throw new CustomSystemError("No such post found", 404);
    }

    const updatedPost: SinglePost = { ...postEntity, isLocked: false };
    const savedPost: SinglePost = await this.singlePostRepo.save(updatedPost);

    return plainToClass(ReturnPostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }
}
