import { Comment } from './../data/entities/comment.entity';
import { Module } from '@nestjs/common';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SinglePost } from '../data/entities/singlePost.entity';
import { User } from '../data/entities/user.entity';
import { Like } from '../data/entities/like.entity';
@Module({
  imports: [TypeOrmModule.forFeature([Comment, SinglePost, User, Like])],
  controllers: [PostsController],
  providers: [PostsService],
})
export class PostsModule {}
