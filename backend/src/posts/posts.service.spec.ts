import { Like } from "../data/entities/like.entity";
import { SinglePost } from "./../data/entities/singlePost.entity";
import { PostsService } from "./posts.service";
import { TestingModule, Test } from "@nestjs/testing";
import { Repository } from "typeorm";
import { getRepositoryToken } from "@nestjs/typeorm";
import { User } from "../data/entities/user.entity";

describe("PostsService", () => {
  let service: PostsService;
  const usersRepo: Partial<Repository<User>> = {};
  const postsRepo: Partial<Repository<SinglePost>> = {};
  const likesRepo: Partial<Repository<Like>> = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostsService,
        {
          provide: getRepositoryToken(User),
          useValue: usersRepo,
        },
        {
          provide: getRepositoryToken(SinglePost),
          useValue: postsRepo,
        },
        {
          provide: getRepositoryToken(Like),
          useValue: likesRepo,
        },
      ],
    }).compile();

    service = module.get<PostsService>(PostsService);

    jest.clearAllMocks();
  });

  // it("should be defined", () => {
  //   expect(service).toBeDefined();
  // });
  // describe("allPosts()", () => {
  //   it("should call singlePostRepo.find once", () => {

  //   });
  // });
});
