import { SinglePost } from "./../data/entities/singlePost.entity";
import { AuthGuard } from "@nestjs/passport";
import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Query,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  Req,
} from "@nestjs/common";
import { PostsService } from "./posts.service";
import { CreatePostDTO } from "../models/post/create-post.dto";
import { ResponseMessageDTO } from "../models/response-message-dto";
import { ReturnPostDTO } from "../models/post/return-post.dto";
import { UpdatePostDTO } from "../models/post/update-post.dto";
import { AdminGuard } from "../common/guards/admin.guard";
import { CustomSystemError } from "../common/exceptions/custum-system.error";

@Controller("posts")
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Get("/categories/:category")
  @HttpCode(HttpStatus.OK)
  public async allPostsCategory(
    @Param("category") category: string
  ): Promise<SinglePost[]> {
    return await this.postsService.allPostsFromCategory(category);
  }

  @Get("/flaggedPosts")
  @HttpCode(HttpStatus.OK)
  public async allFlaggedPosts(): Promise<SinglePost[]> {
    return await this.postsService.flaggedPosts();
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  public async allPosts(@Query("name") name: string): Promise<ReturnPostDTO[]> {
    return await this.postsService.allPostsByName(name);
  }

  @Get("/mostpopular")
  @HttpCode(HttpStatus.OK)
  public async mostPopular(): Promise<SinglePost[]> {
    return await this.postsService.mostPopular();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard("jwt"))
  public async addNewPost(
    @Body() singlePost: CreatePostDTO,
    @Req() request: any
  ): Promise<SinglePost> {
    return await this.postsService.createPost(singlePost, request.user);
  }

  @Put("/:id")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async updatePost(
    @Param("id") postId: string,
    @Body() body: UpdatePostDTO,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    await this.postsService.updatePost(+postId, body, request.user);
    return { msg: "Post Updated!" };
  }

  @Delete("/:id")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async deletePost(
    @Param("id") postId: string,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    await this.postsService.deletePost(+postId, request.user);
    return { msg: "Post Deleted!" };
  }

  @Get("/:id")
  @HttpCode(HttpStatus.OK)
  public async getById(@Param("id") id: string): Promise<SinglePost> {
    const post: SinglePost = await this.postsService.getPostById(+id);
    return post;
  }

  @Put("/:id/flag")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async flagPost(
    @Param("id") postId: string,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    if (!request.user) {
      throw new CustomSystemError("User must be logged.", 401);
    }
    await this.postsService.flagPost(+postId, request.user);
    return { msg: "Post is flagged" };
  }

  @Delete("/:id/flag")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async unFlagPost(
    @Param("id") postId: string
  ): Promise<ResponseMessageDTO> {
    await this.postsService.unflagPost(+postId);
    return { msg: "Post is unflagged" };
  }

  @Post("/:id/likes")
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard("jwt"))
  async createPostLike(
    @Param("id") postId: string,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    if (!request.user) {
      throw new CustomSystemError("User must be logged.", 401);
    }
    await this.postsService.addPostLike(+postId, request.user);
    return { msg: "Like added" };
  }

  @Delete("/:id/likes")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async deletePostLike(
    @Param("id") postId: string,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    await this.postsService.removePostLike(+postId, request.user);
    return { msg: "Like removed" };
  }

  @Put("/:id/lock")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async lockPost(
    @Param("id") postId: number
  ): Promise<ResponseMessageDTO> {
    await this.postsService.lockPost(postId);
    return { msg: "Post is locked!" };
  }

  @Delete("/:id/lock")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async unlockPost(
    @Param("id") postId: number
  ): Promise<ResponseMessageDTO> {
    await this.postsService.unlockPost(postId);
    return { msg: "Post is unlocked!" };
  }
}
