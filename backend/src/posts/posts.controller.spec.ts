import { UpdatePostDTO } from "./../models/post/update-post.dto";
import { CreatePostDTO } from "./../models/post/create-post.dto";
import { PostsService } from "./posts.service";
import { TestingModule, Test } from "@nestjs/testing";
import { PostsController } from "./posts.controller";
import { SinglePost } from "../data/entities/singlePost.entity";

describe("Posts Controller", () => {
  let controller: PostsController;
  let postsService: any;

  beforeEach(async () => {
    postsService = {
      allPosts() {
        /* empty */
      },
      createPost() {
        /* empty */
      },
      updatePost() {
        /* empty */
      },
      deletePost() {
        /* empty */
      },
      getPostById() {
        /* empty */
      },
      flagPost() {
        /* empty */
      },
      unflagPost() {
        /* empty */
      },
      addPostLike() {
        /* empty */
      },
      removePostLike() {
        /* empty */
      },
      lockPost() {
        /* empty */
      },
      unlockPost() {
        /* empty */
      },
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostsController],
      providers: [
        {
          provide: PostsService,
          useValue: postsService,
        },
      ],
    }).compile();

    controller = module.get<PostsController>(PostsController);

    jest.clearAllMocks();
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });

  describe(" allPosts()", () => {
    // it("should call PostsService.allPosts() once with no parameters", async () => {
    //   //Arrange
    //   const filteringName = "name";
    //   const fakePosts = [{ name: "name" }, { name: "post" }];
    //   const spy = jest
    //     .spyOn(postsService, "allPosts")
    //     .mockReturnValue(Promise.resolve(fakePosts));
    //   //Act
    //   await controller.allPosts(filteringName);
    //   //Assert
    //   expect(spy).toBeCalledTimes(1);
    //   expect(spy).toBeCalledWith();
    // });
    // it("should return filtered posts if a filtering name is passed", async () => {
    //   //Arrange
    //   const filteringName = "name";
    //   const fakePosts = [{ name: "name" }, { name: "post" }];
    //   const expectedResult = [{ name: "name" }];
    //   jest
    //     .spyOn(postsService, "allPosts")
    //     .mockReturnValue(Promise.resolve(fakePosts));
    //   //Act
    //   const result = await controller.allPosts(filteringName);
    //   //Assert
    //   expect(result).toEqual(expectedResult);
    // });
    // it("should return all posts if a filtering name is not passed", async () => {
    //   //Arrange
    //   const fakePosts = [{ name: "name" }, { name: "post" }];
    //   jest
    //     .spyOn(postsService, "allPosts")
    //     .mockReturnValue(Promise.resolve(fakePosts));
    //   //Act
    //   const result = await controller.allPosts(undefined);
    //   //Assert
    //   expect(result).toEqual(fakePosts);
    // });
  });

  describe("addNewPost()", () => {
    it("should call createPost() once with correct parameters", async () => {
      //Arrange
      const fakeSinglePost = new CreatePostDTO();
      const fakeRequest = { user: "user" };

      const spy = jest.spyOn(postsService, "createPost");

      //Act
      await controller.addNewPost(fakeSinglePost, fakeRequest);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakeSinglePost, fakeRequest.user);
    });

    it("should return what postsService.createPost() return, ", async () => {
      //Arrange
      const fakeSinglePost = new CreatePostDTO();
      const fakeRequest = { user: "user" };

      //Act
      const result = await controller.addNewPost(fakeSinglePost, fakeRequest);

      //Assert
      // I can't find out why it returns 'undefined'...
      expect(result).toStrictEqual(undefined);
    });
  });
  describe("updatePost()", () => {
    it("should call updatePost() once with correct parameters", async () => {
      //Arrange
      const fakePostId = "1";
      const idToNumber = 1;
      const fakeBody = new UpdatePostDTO();
      const fakeRequest = { user: "user" };

      const spy = jest.spyOn(postsService, "updatePost");

      //Act
      await controller.updatePost(fakePostId, fakeBody, fakeRequest);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(idToNumber, fakeBody, fakeRequest.user);
    });

    it("should return correct msg", async () => {
      //Arrange
      const fakePostId = "1";
      const fakeBody = new UpdatePostDTO();
      const fakeRequest = { user: "user" };
      const expectedResult = { msg: "Post Updated!" };

      //Act
      const result = await controller.updatePost(
        fakePostId,
        fakeBody,
        fakeRequest
      );

      //Assert
      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe("deletePos()", () => {
    it("should call postsService.deletePost() once with correct parameters", async () => {
      //Arrange
      const fakePostId = "1";
      const idToNumber = 1;
      const fakeRequest = { user: "user" };

      const spy = jest.spyOn(postsService, "deletePost");

      //Act
      await controller.deletePost(fakePostId, fakeRequest);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(idToNumber, fakeRequest.user);
    });

    it("should return correct msg", async () => {
      //Arrange
      const fakePostId = "1";
      const fakeRequest = { user: "user" };
      const expectedResult = { msg: "Post Deleted!" };

      //Act
      const result = await controller.deletePost(fakePostId, fakeRequest);

      //Assert
      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe("getById()", () => {
    it("should call getPostById() once with correct parameter", async () => {
      //Arrange
      const fakePostId = "1";
      const idToNumber = 1;

      const spy = jest.spyOn(postsService, "getPostById");

      //Act
      await controller.getById(fakePostId);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(idToNumber);
    });

    it("should return getPostById result", async () => {
      //Arrange
      const fakePostId = "1";
      const fakeResult = "fake value";

      jest.spyOn(postsService, "getPostById").mockReturnValue(fakeResult);

      //Act
      const result = await controller.getById(fakePostId);

      //Assert
      expect(result).toStrictEqual(fakeResult);
    });
  });

  describe("flagPost()", () => {
    it("should call postsService.flagPost() once with correct parameters", async () => {
      //Arrange
      const fakePostId = "1";
      const idToNumber = 1;
      const fakeRequest = { user: "user" };

      const spy = jest.spyOn(postsService, "flagPost");

      //Act
      await controller.flagPost(fakePostId, fakeRequest);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(idToNumber, fakeRequest.user);
    });

    it("should return correct msg", async () => {
      //Arrange
      const fakePostId = "1";
      const fakeRequest = { user: "user" };
      const expectedResult = { msg: "Post is flagged" };

      //Act
      const result = await controller.flagPost(fakePostId, fakeRequest);

      //Assert
      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe("unFlagPost()", () => {
    it("should call postsService.unflagPost() once with correct parameters", async () => {
      //Arrange
      const fakePostId = "1";
      const idToNumber = 1;

      const spy = jest.spyOn(postsService, "unflagPost");

      //Act
      await controller.unFlagPost(fakePostId);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(idToNumber);
    });

    it("should return correct msg", async () => {
      //Arrange
      const fakePostId = "1";
      const expectedResult = { msg: "Post is unflagged" };

      //Act
      const result = await controller.unFlagPost(fakePostId);

      //Assert
      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe("createPostLike()", () => {
    it("should call postsService.addPostLike() once with correct parameters", async () => {
      //Arrange
      const fakePostId = "1";
      const idToNumber = 1;
      const fakeRequest = { user: "user" };

      const spy = jest.spyOn(postsService, "addPostLike");

      //Act
      await controller.createPostLike(fakePostId, fakeRequest);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(idToNumber, fakeRequest.user);
    });

    it("should return correct msg", async () => {
      //Arrange
      const fakePostId = "1";
      const fakeRequest = { user: "user" };
      const expectedResult = { msg: "Like added" };

      //Act
      const result = await controller.createPostLike(fakePostId, fakeRequest);

      //Assert
      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe("deletePostLike()", () => {
    it("should call postsService.aremovePostLike() once with correct parameters", async () => {
      //Arrange
      const fakePostId = "1";
      const idToNumber = 1;
      const fakeRequest = { user: "user" };

      const spy = jest.spyOn(postsService, "removePostLike");

      //Act
      await controller.deletePostLike(fakePostId, fakeRequest);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(idToNumber, fakeRequest.user);
    });

    it("should return correct msg", async () => {
      //Arrange
      const fakePostId = "1";
      const fakeRequest = { user: "user" };
      const expectedResult = { msg: "Like removed" };

      //Act
      const result = await controller.deletePostLike(fakePostId, fakeRequest);

      //Assert
      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe("lockPost()", () => {
    it("should call postsService.lockPost() once with correct parameters", async () => {
      //Arrange
      const fakePostId = 1;

      const spy = jest.spyOn(postsService, "lockPost");

      //Act
      await controller.lockPost(fakePostId);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakePostId);
    });

    it("should return correct msg", async () => {
      //Arrange
      const fakePostId = 1;
      const expectedResult = { msg: "Post is locked!" };

      //Act
      const result = await controller.lockPost(fakePostId);

      //Assert
      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe("unlockPost()", () => {
    it("should call postsService.unlockPost() once with correct parameters", async () => {
      //Arrange
      const fakePostId = 1;

      const spy = jest.spyOn(postsService, "unlockPost");

      //Act
      await controller.unlockPost(fakePostId);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakePostId);
    });

    it("should return correct msg", async () => {
      //Arrange
      const fakePostId = 1;
      const expectedResult = { msg: "Post is unlocked!" };

      //Act
      const result = await controller.unlockPost(fakePostId);

      //Assert
      expect(result).toStrictEqual(expectedResult);
    });
  });
});
