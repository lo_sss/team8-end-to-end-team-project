
import { AuthModule } from './../auth/auth.module';
import { Like } from "./../data/entities/like.entity";
import { User } from "./../data/entities/user.entity";
import { SinglePost } from "./../data/entities/singlePost.entity";
import { CommentsService } from "./comments.service";
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Comment } from "../data/entities/comment.entity";
import { CommentsController } from "./comments.controller";

@Module({
  imports: [TypeOrmModule.forFeature([Comment, SinglePost, User, Like]), AuthModule],
  controllers: [CommentsController],
  providers: [CommentsService]
    
})
export class CommentsModule {}
