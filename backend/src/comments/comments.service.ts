import { CustomSystemError } from "./../common/exceptions/custum-system.error";
import { UpdateCommentDTO } from "./../models/comment/comment-update-dto";
import { SinglePost } from "./../data/entities/singlePost.entity";
import { CreateCommentDTO } from "./../models/comment/comment-create-dto";
import { ReturnCommentDTO } from "./../models/comment/comment-return-dto";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Comment } from "../data/entities/comment.entity";
import { Repository } from "typeorm";
import { Like } from "../data/entities/like.entity";
import { LikeCreateCommentDTO } from "../models/like/like-create-comment-dto";
import { plainToClass } from "class-transformer";
import { ReturnUserDTO } from "src/models/user/user-return-dto";
import { Like as TypeOrmLike } from "typeorm";
import { User } from "../data/entities/user.entity";
@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepo: Repository<Comment>,
    @InjectRepository(User) private readonly usersRepo: Repository<User>,
    @InjectRepository(SinglePost)
    private readonly singlePostRepo: Repository<SinglePost>,
    @InjectRepository(Like)
    private readonly likeRepo: Repository<Like>
  ) {}

  public async allCommentsByContent(
    content: string
  ): Promise<ReturnCommentDTO[]> {
    if (!content) {
      throw new CustomSystemError(
        `No comment found with content ${content}`,
        404
      );
    }

    const filteredComments = await this.commentRepo.find({
      content: TypeOrmLike(`%${content}%`),
      isDeleted: false,
    });

    const filteredCommentsNew = [];
    for (const comment of filteredComments) {
      filteredCommentsNew.push({
        ...comment,
        user: await comment.user,
        singlePost: await comment.singlePost,
      });
    }
    return plainToClass(ReturnCommentDTO, filteredCommentsNew);
  }

  public async getById(id: number): Promise<ReturnCommentDTO> {
    const comment = await this.commentRepo.findOne({ where: { id } });
    if (comment === undefined || comment.isDeleted) {
      throw new CustomSystemError("No such comment found", 404);
    }
    const newComment = {
      ...comment,
      user: await comment.user,
      singlePost: await comment.singlePost,
    };
    return plainToClass(ReturnCommentDTO, newComment);
  }

  public async findById(id: number): Promise<Comment> {
    const comment = await this.commentRepo.findOne({ where: { id } });
    if (comment === undefined || comment.isDeleted) {
      throw new CustomSystemError("No such comment found", 404);
    }
    return comment;
  }

  public async createComment(
    comment: CreateCommentDTO,
    user: ReturnUserDTO,
    postId: number
  ): Promise<ReturnCommentDTO> {
    const commentEntity: Comment = this.commentRepo.create(comment);
    const foundUser: User = await this.usersRepo.findOne({
      id: user.id,
    });
    const foundSinglePost: SinglePost = await this.singlePostRepo.findOne({
      id: postId,
    });

    if (foundUser === undefined || foundUser.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }

    commentEntity.user = Promise.resolve(foundUser);

    if (foundSinglePost === undefined || foundSinglePost.isDeleted) {
      throw new CustomSystemError("No such post found", 404);
    }

    commentEntity.singlePost = Promise.resolve(foundSinglePost);

    await this.commentRepo.save(commentEntity);
    const newComment = {
      ...commentEntity,
      user: await commentEntity.user,
      singlePost: await commentEntity.singlePost,
    };
    return plainToClass(ReturnCommentDTO, newComment);
  }

  public async updateComment(
    id: number,
    comment: UpdateCommentDTO,
    user: ReturnUserDTO
  ): Promise<ReturnCommentDTO> {
    const oldComment: Comment = await this.findById(id);

    const commentUser = await oldComment.user;
    const isUserAdmin = (user.roles as any).find(role => role.name === "Admin")
    if (!isUserAdmin && commentUser.id !== user.id) {
      throw new CustomSystemError("Unauthorized user", 401);
    }
    const entityToUpdate: Comment = { ...oldComment, ...comment };
    const savedComment = await this.commentRepo.save(entityToUpdate);

    return plainToClass(ReturnCommentDTO, savedComment, {
      excludeExtraneousValues: true,
    });
  }

  public async deleteComment(
    id: number,
    user: ReturnUserDTO
  ): Promise<Comment> {
    const foundComment: Comment = await this.findById(id);
    const commentUser = await foundComment.user;
    const isUserAdmin = (user.roles as any).find(role => role.name === "Admin")
    if (!isUserAdmin && commentUser.id !== user.id) {
      throw new CustomSystemError("Unauthorized user", 401);
    }

    const savedComment: Comment = await this.commentRepo.save({
      ...foundComment,
      isDeleted: true,
    });

    return plainToClass(Comment, savedComment, {
      excludeExtraneousValues: true,
    });
  }

  async addCommentLike(id: number, user: ReturnUserDTO): Promise<User> {
    const comment: Comment = await this.commentRepo.findOne({ where: { id } });
    const foundUser: User = await this.usersRepo.findOne({
      where: { id: user.id },
    });
    const likeEnt: Like = await this.likeRepo.findOne({
      where: { userId: user.id, commentId: comment.id },
    });

    if (likeEnt) {
      throw new CustomSystemError("You already liked this comment", 403);
    }
    if (comment === undefined || comment.isDeleted) {
      throw new CustomSystemError("No such comment found", 404);
    }

    const like: LikeCreateCommentDTO = {
      postId: null,
      commentId: comment.id,
    };
    const likeEntity: Like = this.likeRepo.create(like);
    const userId = await user.id;
    const newLike = { ...likeEntity, userId };

    await this.likeRepo.save(newLike);
    return foundUser;
  }

  async removeCommentLike(id: number, user: ReturnUserDTO): Promise<Like> {
    const like: Like = await this.likeRepo.findOne({
      where: { commentId: id },
    });
    const foundComment: Comment = await this.commentRepo.findOne({
      where: { id },
    });

    if (foundComment === undefined || foundComment.isDeleted) {
      throw new CustomSystemError("No such comment found", 404);
    }

    const commentUser = await foundComment.user;
    const isUserAdmin = (user.roles as any).find(role => role.name === "Admin")
    if (!isUserAdmin && commentUser.id !== user.id) {
      throw new CustomSystemError("Unauthorized user", 401);
    }
    const removedLike = { ...like, isDeleted: true };
    await this.likeRepo.save(removedLike);

    return plainToClass(Like, removedLike, {
      excludeExtraneousValues: true,
    });
  }

  private async findLikesByCommentId(id: number): Promise<Like[]> {
    const likes = await this.likeRepo.find({
      where: { commentId: id, isDeleted: false },
    });
    const foundComment: Comment = await this.commentRepo.findOne({
      where: { id },
    });
    if (foundComment === undefined || foundComment.isDeleted) {
      throw new CustomSystemError("No such comment found", 404);
    }
    return likes;
  }
}
