import { User } from "src/data/entities/user.entity";
import { ResponseMessageDTO } from "./../models/response-message-dto";
import { UpdateCommentDTO } from "./../models/comment/comment-update-dto";
import { CreateCommentDTO } from "./../models/comment/comment-create-dto";
import { ReturnCommentDTO } from "./../models/comment/comment-return-dto";
import { CommentsService } from "./comments.service";
import {
  Controller,
  Post,
  Body,
  HttpCode,
  HttpStatus,
  Get,
  Query,
  BadRequestException,
  Put,
  Param,
  Delete,
  UseGuards,
  Req,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { AuthGuard } from "@nestjs/passport";
import { CustomSystemError } from "../common/exceptions/custum-system.error";
@ApiTags("posts/:postId/comments")
@Controller("posts/:postId/comments")
// @UseGuards(AuthGuardWithBlacklisting)
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  @Get("/content")
  @HttpCode(HttpStatus.OK)
  public async getByContent(
    @Query("content") content: string
  ): Promise<ReturnCommentDTO[]> {
    return await this.commentsService.allCommentsByContent(content);
  }

  @Get("/:id")
  @UseGuards(AuthGuard("jwt"))
  @HttpCode(HttpStatus.OK)
  public async getById(@Param("id") id: string): Promise<ReturnCommentDTO> {
    const comment: ReturnCommentDTO = await this.commentsService.getById(+id);

    if (!id) {
      throw new BadRequestException(`No comment found with id ${id}`);
    }

    return comment;
  }

  @Post()
  @UseGuards(AuthGuard("jwt"))
  @HttpCode(HttpStatus.CREATED)
  async create(
    @Body() comment: CreateCommentDTO,
    @Param("postId") postId: number,
    @Req() request: any
  ): Promise<ReturnCommentDTO> {
    return this.commentsService.createComment(comment, request.user, postId);
  }

  @Put("/:id")
  @UseGuards(AuthGuard("jwt"))
  @HttpCode(HttpStatus.OK)
  public async updateComment(
    @Param("id") commentId: string,
    @Body() body: UpdateCommentDTO,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    await this.commentsService.updateComment(+commentId, body, request.user);
    return { msg: "Comment Updated!" };
  }

  @Delete("/:id")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async deleteComment(
    @Param("id") commentId: string,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    await this.commentsService.deleteComment(+commentId, request.user);
    return { msg: "Comment Deleted!" };
  }

  @Post("/:commentId/likes")
  @UseGuards(AuthGuard("jwt"))
  @HttpCode(HttpStatus.CREATED)
  async createCommentLike(
    @Param("commentId") commentId: string,
    @Req() request: any
  ): Promise<User> {
    if (!request.user) {
      throw new CustomSystemError("User must be logged.", 401);
    }
    return await this.commentsService.addCommentLike(+commentId, request.user);
  }

  @Delete("/:commentId/likes")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async deleteCommentLike(
    @Param("commentId") commentId: string,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    await this.commentsService.removeCommentLike(+commentId, request.user);
    return { msg: "Like removed" };
  }
}
