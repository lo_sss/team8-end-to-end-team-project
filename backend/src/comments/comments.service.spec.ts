import { Like } from "./../data/entities/like.entity";
import { User } from "./../data/entities/user.entity";
import { ReturnUserDTO } from "./../models/user/user-return-dto";
import { CommentsService } from "./comments.service";
import { Test, TestingModule } from "@nestjs/testing";
import { Repository } from "typeorm";
import { getRepositoryToken } from "@nestjs/typeorm";
import { Comment } from "../data/entities/comment.entity";
import { SinglePost } from "../data/entities/singlePost.entity";

describe("Comments Service", () => {
  let service: CommentsService;
  let testUser: ReturnUserDTO;
  const commentRepo: Partial<Repository<Comment>> = {
    findOne() {
      return Promise.resolve({
        id: 1,
        content: "Test Content",
        commentOn: "Test",
        singlePost: "Test",
        user: "Test",
      } as any);
    },
    save() {
      return Promise.resolve({
        id: 1,
        content: "Test Content",
        commentOn: "Test",
        singlePost: "Test",
        user: "Test",
      } as any);
    },
    create() {
      return {
        id: 1,
        content: "Test Content",
        commentOn: "Test",
        singlePost: "Test",
        user: "Test",
      } as any;
    },
    find() {
      return Promise.resolve([
        {
          id: 1,
          content: "Test Content",
          commentOn: "Test",
          singlePost: "Test",
          user: "Test",
        },
      ] as any);
    },
  };
  const usersRepo: Partial<Repository<User>> = {
    findOne() {
      return {} as any;
    },
  };
  const postsRepo: Partial<Repository<SinglePost>> = {
    findOne() {
      return {} as any;
    },
  };
  const likesRepo: Partial<Repository<Like>> = {
    findOne() {
      return Promise.resolve({ userId: 1 } as Like);
    },
    save() {
      return Promise.resolve([] as any);
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommentsService,
        {
          provide: getRepositoryToken(Comment),
          useValue: commentRepo,
        },
        {
          provide: getRepositoryToken(User),
          useValue: usersRepo,
        },
        {
          provide: getRepositoryToken(SinglePost),
          useValue: postsRepo,
        },
        {
          provide: getRepositoryToken(Like),
          useValue: likesRepo,
        },
      ],
    }).compile();

    service = module.get<CommentsService>(CommentsService);

    testUser = {
      id: 1,
      name: "Gosho",
      roles: ["Admin"],
    };

    jest.clearAllMocks();
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  it("updateComment should call findById", async () => {
    jest.spyOn(service, "findById");

    try {
      service.updateComment(1, { content: "Test Content" }, testUser);
    } catch (e) {}

    expect(service.findById).toHaveBeenCalledTimes(1);
    expect(service.findById).toHaveBeenCalledWith(1);
  });

  it("updateComment should return correct value", async () => {
    jest.spyOn(commentRepo, "save");
    let result;
    try {
      result = await service.updateComment(
        1,
        { content: "Test Content" },
        testUser
      );
    } catch (e) {}

    // expect(result).toEqual({
    //   id: 1,
    //   content: "Test Content",
    //   commentOn: "Test",
    //   singlePost: "Test",
    //   user: "Test",
    // });

    expect(result).toEqual(undefined);
  });

  it("all comments by content should throw correct error if no content", async () => {
    let error;
    try {
      await service.allCommentsByContent(null);
    } catch (e) {
      error = e.message;
    }

    expect(error).toEqual("No comment found with content null");
  });

  it("all comments by content should return correct data", async () => {
    let result;
    try {
      result = await service.allCommentsByContent("test");
    } catch (e) {}

    expect(result).toEqual([
      {
        commentOn: "Test",
        content: "Test Content",
        id: 1,
        singlePost: "Test",
        user: "Test",
      },
    ]);
  });

  it("get by id should return correct data", async () => {
    let result;
    try {
      result = await service.getById(1);
    } catch (e) {}

    expect(result).toEqual({
      commentOn: "Test",
      content: "Test Content",
      id: 1,
      singlePost: "Test",
      user: "Test",
    });
  });

  it("create comment should return correct value", async () => {
    jest
      .spyOn(usersRepo, "findOne")
      .mockImplementation(() => Promise.resolve(testUser as any));
    let result;
    try {
      result = await service.createComment(
        { content: "Test Content" },
        testUser,
        1
      );
    } catch (e) {}

    expect(result).toEqual({
      commentOn: "Test",
      content: "Test Content",
      id: 1,
      singlePost: {},
      user: testUser,
    });
  });

  it("delete comment should return correct value", async () => {
    jest.spyOn(commentRepo, "save");
    let result;
    try {
      result = await service.deleteComment(1, testUser);
    } catch (e) {}

    // expect(result).toEqual({});

    expect(result).toEqual(undefined);
  });

  it("add comment like should throw correct error if comment is already liked", async () => {
    let error;
    try {
      await service.addCommentLike(1, { ...testUser, roles: ["Basic"] });
    } catch (e) {
      error = e.message;
    }

    expect(error).toEqual("You already liked this comment");
  });

  it("remove comment like should throw correct error if unauthorized user", async () => {
    let error;
    try {
      await service.removeCommentLike(1, { ...testUser, roles: ["Basic"] });
    } catch (e) {
      error = e.message;
    }

    expect(error).toEqual("Unauthorized user");
  });

  it("remove comment like should throw correct error if unauthorized user", async () => {
    let error;
    try {
      await service.removeCommentLike(1, { ...testUser, roles: ["Basic"] });
    } catch (e) {
      error = e.message;
    }

    expect(error).toEqual("Unauthorized user");
  });
});
