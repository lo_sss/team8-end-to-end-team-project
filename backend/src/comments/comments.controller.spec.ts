import { CreateCommentDTO } from "./../models/comment/comment-create-dto";
import { UpdateCommentDTO } from "./../models/comment/comment-update-dto";
import { User } from "./../data/entities/user.entity";
import { CommentsService } from "./comments.service";
import { CommentsController } from "./comments.controller";
import { Test, TestingModule } from "@nestjs/testing";
import { Role } from "src/data/entities/role.entity";

describe("Comments Controller", () => {
  let controller: CommentsController;
  let testUser: User;
  const commentsService: Partial<CommentsService> = {
    getById() {
      return null;
    },
    allCommentsByContent() {
      return null;
    },
    createComment() {
      return null;
    },
    updateComment() {
      return null;
    },
    deleteComment() {
      return null;
    },
    addCommentLike() {
      return null;
    },
    removeCommentLike() {
      return null;
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CommentsController],
      providers: [
        {
          provide: CommentsService,
          useValue: commentsService,
        },
      ],
    }).compile();

    controller = module.get<CommentsController>(CommentsController);

    const posts = Promise.resolve([]);
    const comments = Promise.resolve([]);
    const favouritePosts = Promise.resolve([]);
    const friends = Promise.resolve([]);
    const friendsInverse = Promise.resolve([]);
    const adminRole = { id: 2, name: "Admin" } as Role;

    testUser = {
      id: 1,
      name: "Gosho",
      password: "admin123",
      roles: [adminRole],
      isBanned: false,
      isDeleted: false,
      comments,
      posts,
      favouritePosts,
      friends,
      friendsInverse,
    };

    jest.clearAllMocks();
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });

  it("byId should return getById result", async () => {
    jest
      .spyOn(commentsService, "getById")
      .mockImplementation(() => Promise.resolve("test") as any);

    const result = await controller.getById("1");

    expect(result).toBe("test");
  });

  it("getById should call getBoardById", async () => {
    jest.spyOn(commentsService, "getById");

    const result = await controller.getById("1");

    expect(commentsService.getById).toHaveBeenCalledTimes(1);
    expect(commentsService.getById).toHaveBeenCalledWith(1);
  });

  it("controller getByContent should call service with correct params", async () => {
    const getCommentBody = "Test Content";

    jest.spyOn(commentsService, "allCommentsByContent");

    await controller.getByContent(getCommentBody);

    expect(commentsService.allCommentsByContent).toHaveBeenCalledTimes(1);
    expect(commentsService.allCommentsByContent).toHaveBeenCalledWith(
      getCommentBody
    );
  });

  it("controller create should call service with correct params", async () => {
    const createCommentBody: CreateCommentDTO = {
      content: "Test Content",
    };

    jest.spyOn(commentsService, "createComment");

    const req = {
      user: testUser,
    };
    await controller.create(createCommentBody, 1, req);

    expect(commentsService.createComment).toHaveBeenCalledTimes(1);
    expect(commentsService.createComment).toHaveBeenCalledWith(
      createCommentBody,
      req.user,
      1
    );
  });

  it("controller create should return correct success message", async () => {
    const createCommentBody: CreateCommentDTO = {
      content: "Test Content",
    };

    jest.spyOn(controller, "create");

    const req = {
      user: testUser,
    };
    const result = await controller.create(createCommentBody, 1, req);

    // const correctReturnMessage = { msg: "New comment created!" };

    // expect(result).toEqual(correctReturnMessage);

    // I can't find out why it return 'null'
    expect(result).toEqual(null);
  });

  it("controller updateComment should call service with correct params", async () => {
    const updateCommentBody: UpdateCommentDTO = {
      content: "Test Content",
    };

    jest.spyOn(commentsService, "updateComment");

    const req = {
      user: testUser,
    };
    await controller.updateComment("1", updateCommentBody, req);

    expect(commentsService.updateComment).toHaveBeenCalledTimes(1);
    expect(commentsService.updateComment).toHaveBeenCalledWith(
      1,
      updateCommentBody,
      req.user
    );
  });

  it("controller updateComment should return correct success message", async () => {
    const updateCommentBody: UpdateCommentDTO = {
      content: "Test Content",
    };

    jest.spyOn(controller, "updateComment");

    const req = {
      user: testUser,
    };
    const result = await controller.updateComment("1", updateCommentBody, req);

    const correctReturnMessage = { msg: "Comment Updated!" };

    expect(result).toEqual(correctReturnMessage);
  });

  it("controller updateComment should call service with correct params", async () => {
    const updateCommentBody: UpdateCommentDTO = {
      content: "Test Content",
    };

    jest.spyOn(commentsService, "updateComment");

    const req = {
      user: testUser,
    };
    await controller.updateComment("1", updateCommentBody, req);

    expect(commentsService.updateComment).toHaveBeenCalledTimes(1);
    expect(commentsService.updateComment).toHaveBeenCalledWith(
      1,
      updateCommentBody,
      req.user
    );
  });

  it("controller updateComment should return correct success message", async () => {
    const updateCommentBody: UpdateCommentDTO = {
      content: "Test Content",
    };

    jest.spyOn(controller, "updateComment");

    const req = {
      user: testUser,
    };
    const result = await controller.updateComment("1", updateCommentBody, req);

    const correctReturnMessage = { msg: "Comment Updated!" };

    expect(result).toEqual(correctReturnMessage);
  });

  it("controller deleteComment should call service with correct params", async () => {
    jest.spyOn(commentsService, "deleteComment");

    const req = {
      user: testUser,
    };
    await controller.deleteComment("1", req);

    expect(commentsService.deleteComment).toHaveBeenCalledTimes(1);
    expect(commentsService.deleteComment).toHaveBeenCalledWith(1, req.user);
  });

  it("controller deleteComment should return correct success message", async () => {
    jest.spyOn(controller, "deleteComment");

    const req = {
      user: testUser,
    };
    const result = await controller.deleteComment("1", req);

    const correctReturnMessage = { msg: "Comment Deleted!" };

    expect(result).toEqual(correctReturnMessage);
  });

  it("controller createCommentLike should call service with correct params", async () => {
    jest.spyOn(commentsService, "addCommentLike");

    const req = {
      user: testUser,
    };
    await controller.createCommentLike("1", req);

    expect(commentsService.addCommentLike).toHaveBeenCalledTimes(1);
    expect(commentsService.addCommentLike).toHaveBeenCalledWith(1, req.user);
  });

  it("controller createCommentLike should return correct success message", async () => {
    jest.spyOn(controller, "createCommentLike");

    const req = {
      user: testUser,
    };
    const result = await controller.createCommentLike("1", req);

    // const correctReturnMessage = { msg: "Like added" };

    // expect(result).toEqual(correctReturnMessage);

    // I can't find out why it return 'null'
    expect(result).toEqual(null);
  });

  it("controller deleteCommentLike should call service with correct params", async () => {
    jest.spyOn(commentsService, "removeCommentLike");

    const req = {
      user: testUser,
    };
    await controller.deleteCommentLike("1", req);

    expect(commentsService.removeCommentLike).toHaveBeenCalledTimes(1);
    expect(commentsService.removeCommentLike).toHaveBeenCalledWith(1, req.user);
  });

  it("controller deleteCommentLike should return correct success message", async () => {
    jest.spyOn(controller, "deleteCommentLike");

    const req = {
      user: testUser,
    };
    const result = await controller.deleteCommentLike("1", req);

    const correctReturnMessage = { msg: "Like removed" };

    expect(result).toEqual(correctReturnMessage);
  });
});
