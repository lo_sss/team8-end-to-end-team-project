import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from "./database/database.module";
import { Module } from "@nestjs/common";
import { PostsModule } from "./posts/posts.module";
import { CommentsModule } from "./comments/comments.module";
import { UsersModule } from "./users/users.module";
import { ConfigModule } from '@nestjs/config';
import Joi = require("@hapi/joi");
import { APP_INTERCEPTOR } from "@nestjs/core";
import { BannedUserInterceptor } from "./common/interceptors/banned-users-interceptor";

@Module({
  imports: [
    UsersModule,
    DatabaseModule,
    PostsModule,
    CommentsModule,
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        PORT: Joi.number().default(3000),
        DB_TYPE: Joi.string().required(),
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_USERNAME: Joi.string().required(),
        DB_PASSWORD: Joi.string().required(),
        DB_DATABASE_NAME: Joi.string().required()
      }),
    }),
    AuthModule
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: BannedUserInterceptor,
    },
  ],
  exports: [AuthModule, ConfigModule]
})
export class AppModule {}
