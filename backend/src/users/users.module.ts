import { Like } from "../data/entities/like.entity";
import { Role } from "./../data/entities/role.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./../data/entities/user.entity";
import { SinglePost } from "./../data/entities/singlePost.entity";
import { Comment } from "./../data/entities/comment.entity";
import { Module } from "@nestjs/common";
import { UsersController } from "./users.controller";
import { UsersService } from "./users.service";

@Module({
  imports: [TypeOrmModule.forFeature([Comment, SinglePost, User, Role, Like])],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService]
})
export class UsersModule {}
