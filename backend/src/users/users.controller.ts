import { SinglePost } from "./../data/entities/singlePost.entity";
import { AuthGuard } from "@nestjs/passport";
import { AdminGuard } from "./../common/guards/admin.guard";
import { UpdateUserRolesDTO } from "./../models/user/user-update-roles.dto";
import { ResponseMessageDTO } from "./../models/response-message-dto";
import { UpdateUserDTO } from "./../models/user/user-update-dto";
import { CreateUserDTO } from "./../models/user/user-create-dto";
import { ReturnUserDTO } from "./../models/user/user-return-dto";
import { UsersService } from "./users.service";
import { ApiTags } from "@nestjs/swagger";
import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  BadRequestException,
  Query,
  Post,
  Body,
  Put,
  Delete,
  UseGuards,
  Req,
} from "@nestjs/common";
import { ProfileUserDTO } from "../models/user/user-profile-dto";
import { CustomSystemError } from "../common/exceptions/custum-system.error";
@ApiTags("users")
@Controller("users")
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get("/name")
  @HttpCode(HttpStatus.OK)
  public async getByName(
    @Query("name") name: string
  ): Promise<ReturnUserDTO[]> {
    return await this.usersService.allUsersByName(name);
  }

  @Get("/:id")
  @HttpCode(HttpStatus.OK)
  public async getById(@Param("id") id: string): Promise<ReturnUserDTO> {
    const user: ReturnUserDTO = await this.usersService.findUserById(+id);

    if (!id) {
      throw new BadRequestException(`No user found with id ${id}`);
    }

    return user;
  }

  @Get("/:id/favposts")
  @HttpCode(HttpStatus.OK)
  public async getUserFav(@Param("id") id: string): Promise<SinglePost[]> {
    return await this.usersService.findUserFav(+id);
  }

  @Get("/profile/:id")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async getProfile(
    @Param("id") id: string,
    @Req() request: any
  ): Promise<ProfileUserDTO> {
    if (!id) {
      throw new BadRequestException(`No user found with id ${id}`);
    }

    const user: ProfileUserDTO = await this.usersService.getProfile(
      +id,
      request.user
    );

    return user;
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() user: CreateUserDTO): Promise<ResponseMessageDTO> {
    await this.usersService.createUser(user);
    return { msg: "User created!" };
  }

  @Put("/:id")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async updateaUser(
    @Param("id") userId: string,
    @Body() user: UpdateUserDTO,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    await this.usersService.updateUser(+userId, user, request.user);
    return { msg: "User Updated!" };
  }

  @Delete("/:id")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async deleteUser(
    @Param("id") userId: string
  ): Promise<ResponseMessageDTO> {
    await this.usersService.deleteUser(+userId);
    return { msg: "User Deleted!" };
  }

  @Put("/friends/:userId")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async addFriend(
    @Param("userId") userId: string,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    console.log("HERE");
    await this.usersService.addFriend(+userId, request.user);
    return { msg: "Friend added!" };
  }

  @Delete("/friends/:userId")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async deleteUserFriend(
    @Param("userId") userId: string,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    await this.usersService.removeFriend(+userId, request.user);
    return { msg: "Friend removed" };
  }

  @Put("/:userId/favouritePosts/:postId")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async makeFavourite(
    @Param("userId") userId: string,
    @Param("postId") postId: string,
    @Req() request: any
  ): Promise<SinglePost> {
    if (!request.user) {
      throw new CustomSystemError("User must be logged.", 401);
    }
    return await this.usersService.addToFavourites(
      +userId,
      +postId,
      request.user
    );
  }

  @Delete("/:userId/favouritePosts/:postId")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard("jwt"))
  public async removeFavourite(
    @Param("userId") userId: string,
    @Param("postId") postId: string,
    @Req() request: any
  ): Promise<ResponseMessageDTO> {
    await this.usersService.removeFromFavourites(
      +userId,
      +postId,
      request.user
    );
    return { msg: "Post removed from favourites" };
  }

  @Put("/:id/roles")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async updateUserRoles(
    @Param("id") userId: string,
    @Body() updateUserRoles: UpdateUserRolesDTO
  ): Promise<ReturnUserDTO> {
    return await this.usersService.updateUserRoles(+userId, updateUserRoles);
  }

  @Put("/:id/ban/:period")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async banUser(
    @Param("id") userId: number,
    @Param("period") period: number
  ): Promise<ResponseMessageDTO> {
    await this.usersService.banUser(userId, period);
    return { msg: "User is banned!" };
  }

  @Delete("/:id/ban")
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async unbanUser(
    @Param("id") userId: number
  ): Promise<ResponseMessageDTO> {
    await this.usersService.unbanUser(userId);
    return { msg: "User is not baned!" };
  }

  // @Post('/:id/avatar')
  // @HttpCode(HttpStatus.CREATED)
  // @UseInterceptors(FileInterceptor('file'))
  // public async uploadUserAvatar(
  //   @Param('id') id: string,
  //   @UploadedFile() file: any,
  // ): Promise<ReturnUserDTO> {
  //   const updateUserProperties: Partial<UpdateUserDTO> = {
  //     avatarUrl: file?.filename,
  //   };

  //   return await this.usersService.updateUser(+id, updateUserProperties);
  // }
}
