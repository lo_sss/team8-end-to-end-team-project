import { CustomSystemError } from "./../common/exceptions/custum-system.error";
import { UpdateUserRolesDTO } from "./../models/user/user-update-roles.dto";
import { UpdateUserDTO } from "./../models/user/user-update-dto";
import { ReturnUserDTO } from "./../models/user/user-return-dto";
import { SinglePost } from "./../data/entities/singlePost.entity";
import { User } from "./../data/entities/user.entity";
import { Comment } from "./../data/entities/comment.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Injectable } from "@nestjs/common";
import { Repository, In } from "typeorm";
import { CreateUserDTO } from "src/models/user/user-create-dto";
import { Role } from "../data/entities/role.entity";
import { Like } from "src/data/entities/like.entity";
import { ProfileUserDTO } from "src/models/user/user-profile-dto";
import { plainToClass } from "class-transformer";
import * as bcrypt from "bcrypt";
import { Like as TypeOrmLike } from "typeorm";
import moment = require("moment");

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepo: Repository<Comment>,
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
    @InjectRepository(Like)
    private readonly likeRepo: Repository<Like>,
    @InjectRepository(SinglePost)
    private readonly singlePostRepo: Repository<SinglePost>
  ) {}

  public async allUsersByName(name): Promise<ReturnUserDTO[]> {
    if (!name) {
      throw new CustomSystemError(`No user found with name ${name}`, 404);
    }

    const filteredUsers = await this.userRepo.find({
      name: TypeOrmLike(`%${name}%`),
      isDeleted: false
    });

    const usersNew = [];

    for (const user of filteredUsers) {
      const { ...userWithoutPass } = user;
      usersNew.push({
        ...userWithoutPass,
        posts: await user.posts,
        comments: await user.comments
      });
    }

    return plainToClass(ReturnUserDTO, usersNew);
  }

  public async findUserById(id: number): Promise<ReturnUserDTO> {
    const user = await this.userRepo.findOne({ where: { id } });
    console.log(user)
    if (user === undefined || user.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    const newUser = {
      ...user,
      comments: await user.comments,
      posts: await user.posts
    };
    return plainToClass(ReturnUserDTO, newUser);
  }

  public async findUserFav(id: number): Promise<SinglePost[]> {
    const user = await this.userRepo.findOne({ where: { id } });
    if (user === undefined || user.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    const newFavouritePosts = await user.favouritePosts
    
    return newFavouritePosts
  }

  public async createUser(user: CreateUserDTO): Promise<ReturnUserDTO> {
    const roleEntity: Role = await this.rolesRepository.findOne({
      where: { name: "Basic" }
    });

    const foundUser: User = await this.userRepo.findOne({
      name: user.name
    });
    if (foundUser) {
      throw new CustomSystemError(
        "User with such username already exists!",
        400
      );
    }

    const userEntity: User = this.userRepo.create(user);
    // await this.rolesRepository.create(roleEntity)
  
    userEntity.roles = [roleEntity];
    userEntity.password = await bcrypt.hash(userEntity.password, 10);
    const savedUser = await this.userRepo.save(userEntity);
    const newUser = {
      ...savedUser,
      comments: await savedUser.comments,
      posts: await savedUser.posts
    };
    // return plainToClass(ReturnUserDTO, newUser);
    return (newUser as any);
  }

  public async updateUser(
    id: number,
    user: UpdateUserDTO,
    loggedUser: ReturnUserDTO
  ): Promise<ReturnUserDTO> {
    const oldUser: User = await this.userRepo.findOne({ where: { id } });

    if (oldUser === undefined || oldUser.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    if (oldUser.id !== loggedUser.id) {
      throw new CustomSystemError("Unauthorized user", 401);
    }
    const entityToUpdate: User = { ...oldUser, ...user };

    const savedUser = await this.userRepo.save(entityToUpdate);
    return plainToClass(ReturnUserDTO, savedUser, {
      excludeExtraneousValues: true
    });
  }

  public async updateUserRoles(
    id: number,
    updateUserRoles: UpdateUserRolesDTO
  ): Promise<ReturnUserDTO> {
    const foundUser: User = await this.userRepo.findOne({ id });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new CustomSystemError(
        "User you want to promote does not exist!",
        404
      );
    }

    const roleEntities: Role[] = await this.rolesRepository.find({
      where: {
        name: In(updateUserRoles.roles)
      }
    });

    const entityToUpdate: User = { ...foundUser, roles: roleEntities };
    const savedUser: User = await this.userRepo.save(entityToUpdate);

    return plainToClass(ReturnUserDTO, savedUser, {
      excludeExtraneousValues: true
    });
  }

  public async deleteUser(id: number): Promise<User> {
    const foundUser: User = await this.userRepo.findOne({ where: { id } });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    const deletedUser = this.userRepo.save({ ...foundUser, isDeleted: true });
    return plainToClass(User, deletedUser, {
      excludeExtraneousValues: true
    });
  }

  public async getProfile(
    id: number,
    loggedUser: ReturnUserDTO
  ): Promise<ProfileUserDTO> {
    const user = await this.userRepo.findOne({ where: { id } });
    
    if (user === undefined || user.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    const likedPosts = await this.likeRepo.find({
      where: { userId: id, commentId: null }
    });
    const likedComments = await this.likeRepo.find({
      where: { userId: id, postId: null }
    });


    const likedCommentsNew = [];
    const likedPostNew = [];

    for (const { commentId } of likedComments) {
      const comment = await this.commentRepo.findOne({ where: { id: commentId } });
      const user = await comment.user;
      likedCommentsNew.push({ ...comment, userId: user.id,  username: user.name });
    }

    for (const { postId } of likedPosts) {
      const post = await this.singlePostRepo.findOne({ where: { id: postId } });
      const user = await post.user
      likedPostNew.push({ ...post, userId: user.id, username: user.name });
    }


    // const flaggedPosts = await this.singlePostRepo.find({
    //   where: { isDeleted: false, isFlagged: true }
    // });

    // const bannedUsers = await this.userRepo.find({
    //   where: { isDeleted: false, isBanned: true }
    // });

    const isAdmin = loggedUser.roles.some(role => (role as any).id === 2);

    let newUser;

    if (id === loggedUser.id || isAdmin) {
      newUser =
        loggedUser.id === user.id
          ? {
              ...user,
              comments: await user.comments,
              posts: (await user.posts) || [],
              friends: await user.friends,
              likedPosts: likedPostNew,
              likedComments: likedCommentsNew,
              // flaggedPosts,
              // bannedUsers,
            }
          : {
              ...user,
              comments: await user.comments,
              posts: (await user.posts) || [],
              friends: await user.friends,
              likedPosts: likedPostNew,
              likedComments: likedCommentsNew
            };
    } else {
      const areFriends = (await user.friends).find(
        friend => friend.id === loggedUser.id
      );
      newUser = areFriends
        ? {
            ...user,
            friends: await user.friends,
            likedPosts: likedPostNew,
            likedComments: likedCommentsNew
          }
        : {
            name: user.name,
            friends: await user.friends,
          };
    }

    // return plainToClass(ProfileUserDTO, newUser);
    return newUser
  }

  public async findLikesByUserId(id: number): Promise<Like[]> {
    const likes = await this.likeRepo.find({
      where: { userId: id, isDeleted: false }
    });
    const user = await this.userRepo.findOne({ where: { id } });
    if (user === undefined || user.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    return likes;
  }

  public async findFriendById(id: number): Promise<ReturnUserDTO> {
    const user = await this.userRepo.findOne({ where: { id } });
    if (user === undefined || user.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    return plainToClass(ReturnUserDTO, user, {
      excludeExtraneousValues: true
    });
  }

  public async addFriend(
    userId: number,
    loggedUser
  ): Promise<User> {
    const user: User = await this.userRepo.findOne({ where: { id: loggedUser.id } });
    if (user === undefined || user.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    const userFriend: User = await this.userRepo.findOne({
      where: { id: userId }
    });
    if (userFriend === undefined || userFriend.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    (await user.friends).push(userFriend);
    (await userFriend.friends).push(user);
    this.userRepo.save(user);
    this.userRepo.save(userFriend);
    return plainToClass(User, user, {
      excludeExtraneousValues: true
    });
  }

  public async removeFriend(
    userId: number,
    loggedUser: ReturnUserDTO
  ): Promise<User> {
    const foundUser: User = await this.userRepo.findOne({ where: { id: loggedUser.id } });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    const index = (await foundUser.friends).findIndex(
      friend => friend.id === userId
    );

    (await foundUser.friends).splice(index, 1);

    const foundSecondUser: User = await this.userRepo.findOne({
      where: { id: userId }
    });
    if (foundSecondUser === undefined || foundSecondUser.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    const index2 = (await foundSecondUser.friends).findIndex(
      friend => friend.id === loggedUser.id
    );
    (await foundSecondUser.friends).splice(index2, 1);

    this.userRepo.save(foundUser);
    this.userRepo.save(foundSecondUser);

    return plainToClass(User, foundUser, {
      excludeExtraneousValues: true
    });
  }

  public async addToFavourites(
    userId: number,
    postId: number,
    loggedUser: ReturnUserDTO
  ): Promise<SinglePost> {
    const foundUser: User = await this.userRepo.findOne({
      where: { id: loggedUser.id, isDeleted: false }
    });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }

    const foundPost: SinglePost = await this.singlePostRepo.findOne({
      where: { id: postId, isDeleted: false }
    });
    if (foundPost === undefined || foundPost.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }

    const alreadyLiked = await (await foundUser.favouritePosts).find(post => post.id === foundPost.id)
    if (alreadyLiked) {
      throw new CustomSystemError("You already added this post to favourite", 403);
    }
    (await foundUser.favouritePosts).push(foundPost);
    await this.userRepo.save(foundUser);
    return foundPost
  }

  public async removeFromFavourites(
    userId: number,
    postId: number,
    loggedUser: ReturnUserDTO
  ): Promise<User> {
    const foundUser: User = await this.userRepo.findOne({
      where: { id: userId, isDeleted: false }
    });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new CustomSystemError("No such user found", 404);
    }
    const isUserAdmin = (loggedUser.roles as any).find(role => role.name === "Admin")
    if (!isUserAdmin && foundUser.id !== loggedUser.id) {
      throw new CustomSystemError("Unauthorized user", 401);
    }
    const index = (await foundUser.favouritePosts).findIndex(
      post => post.id === postId
    );
    (await foundUser.favouritePosts).splice(index, 1);
    const savedUser = this.userRepo.save(foundUser);
    return plainToClass(User, savedUser, {
      excludeExtraneousValues: true
    });
  }

  public async banUser(userId: number, period: number): Promise<ReturnUserDTO> {
    const userEntity: User = await this.userRepo.findOne({
      where: { id: userId, isDeleted: false }
    });

    if (!userEntity) {
      throw new CustomSystemError("User you want to ban does not exist!", 404);
    }

    const bannedUntil = moment().add(period, 'days').toDate();

    const updatedUser: User = { ...userEntity, isBanned: true, bannedDate: new Date(), bannedUntil };
    const savedUser: User = await this.userRepo.save(updatedUser);

    return plainToClass(ReturnUserDTO, savedUser, {
      excludeExtraneousValues: true
    });
  }

  public async unbanUser(userId: number): Promise<ReturnUserDTO> {
    const userEntity: User = await this.userRepo.findOne({
      where: { id: userId, isDeleted: false }
    });

    if (!userEntity) {
      throw new CustomSystemError(
        "User you want to remove ban does not exist!",
        404
      );
    }

    const updatedUser: User = { ...userEntity, isBanned: false };
    const savedUser: User = await this.userRepo.save(updatedUser);

    return plainToClass(ReturnUserDTO, savedUser, {
      excludeExtraneousValues: true
    });
  }
}
