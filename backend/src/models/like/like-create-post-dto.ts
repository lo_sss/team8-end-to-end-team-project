import { IsNumber } from "class-validator";

export class LikeCreatePostDTO {
  @IsNumber()
  postId: number;
}