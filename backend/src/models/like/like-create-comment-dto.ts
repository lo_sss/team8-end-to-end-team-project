import { IsNumber } from "class-validator";

export class LikeCreateCommentDTO {
  @IsNumber()
  postId: number;
  
  @IsNumber()
  commentId: number
}