import { SinglePost } from "../../data/entities/singlePost.entity";
import { Expose } from "class-transformer";

export class ReturnUserFavDTO {
  @Expose()
  favouritePosts: Promise<SinglePost[]>;
}
