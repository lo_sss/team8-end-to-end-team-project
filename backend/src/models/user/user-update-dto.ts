import { Length, IsString, IsOptional } from 'class-validator';

export class UpdateUserDTO {
  @IsOptional()
  @Length(0, 20)
  name: string;

  @IsOptional()
  @IsString()
  password: string;

  @IsOptional()
  @IsString()
  public avatarUrl: string;
}
