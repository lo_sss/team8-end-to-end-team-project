import { UserRole } from './enum/user-role.enum';
import { IsArray } from 'class-validator';


export class UpdateUserRolesDTO {
  @IsArray()
  public roles: UserRole[];
}
