export class ShowUserDTO {
  public name: string;
  public roles: string[];
}
