import { Expose, Transform } from "class-transformer";
export class ReturnUserDTO {
  @Expose()
  id: number;

  @Expose()
  name: string;

  @Expose()
  @Transform((_, obj) => obj.roles.map((x: any) => x.name))
  public roles: string[];
}
