export enum PostCategory {
  Animals = "Animals",
  Fun = "Fun",
  Games = "Games",
  History = "History",
  Math = "Math",
  Music = "Music",
  Other = "Other",
  Plants = "Plants",
  TelerikAcademy = "Telerik Academy"
}
