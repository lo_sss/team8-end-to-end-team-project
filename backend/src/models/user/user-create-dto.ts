import { Length, Matches } from 'class-validator';
export class CreateUserDTO {
  @Length(0, 20)
  name: string;

  @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/, {
    message:
      'The password must be minimum five characters, at least five letters and one number',
  })
  public password: string;
}
