import { SinglePost } from './../../data/entities/singlePost.entity';
import { Comment } from './../../data/entities/comment.entity';
import { Expose } from 'class-transformer';
export class ProfileUserDTO {
  @Expose()
  id: number;

  @Expose()
  name: string;

  @Expose()
  comments: Comment[];

  @Expose()
  posts: SinglePost[];

  @Expose()
  likedComments: Comment[];

  @Expose()
  likedPosts: SinglePost[]
  }