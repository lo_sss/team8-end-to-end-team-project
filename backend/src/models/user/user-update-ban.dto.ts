import { IsBoolean } from "class-validator";

export class UpdateUserBanDTO {
  @IsBoolean()
  public isBanned: boolean;
}
