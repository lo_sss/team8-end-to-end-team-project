import { IsString, Length } from 'class-validator';

export class CreatePostDTO {
  @Length(2, 1000)
  @IsString()
  public name: string;

  @Length(2, 1000)
  @IsString()
  public content: string;

  @IsString()
  public category: string
}
