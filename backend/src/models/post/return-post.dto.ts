import { User } from './../../data/entities/user.entity';
import { Expose } from 'class-transformer';
export class ReturnPostDTO {
  @Expose()
  public id: number;

  @Expose()
  public name: string;

  @Expose()
  public content: string;

  @Expose()
  public postedOn: Date;

  @Expose()
  public updatedOn: Date;

  @Expose()
  public isFlagged: boolean;

  @Expose()
  public category: string

  @Expose()
  public user: User

  @Expose()
  public comments: Comment[]
}
