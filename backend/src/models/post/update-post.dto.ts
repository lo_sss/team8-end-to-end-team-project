import { IsOptional, Length, IsString, IsNumber } from "class-validator";

export class UpdatePostDTO {
  @IsOptional()
  @Length(2, 1000)
  public name: string;

  @IsOptional()
  @Length(2, 1000)
  public content: string;

  @IsOptional()
  @IsString()
  public category: string

}
