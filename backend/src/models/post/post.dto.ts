import { Expose } from "class-transformer";

export class PostDTO {
  @Expose()
  public id: number;

  @Expose()
  public name: string;

  @Expose()
  public content: string;

  @Expose()
  public postedOn: Date;

  @Expose()
  public updatedOn: Date;

  @Expose()
  public isFlagged: boolean;
  
  public isDeleted: boolean;
}
