import { Length, IsString } from 'class-validator';

export class CreateCommentDTO {
    @Length(2, 1000)
    @IsString()
    public content: string
}
