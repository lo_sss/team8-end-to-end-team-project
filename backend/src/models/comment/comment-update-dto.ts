import { IsOptional, Length } from 'class-validator';

export class UpdateCommentDTO {
  @IsOptional()
  @Length(2, 1000)
  content: string;
}
