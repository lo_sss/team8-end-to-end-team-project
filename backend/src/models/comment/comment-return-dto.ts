import { SinglePost } from './../../data/entities/singlePost.entity';
import { User } from './../../data/entities/user.entity';
import { Expose, Exclude, Transform, plainToClass } from 'class-transformer';
export class ReturnCommentDTO {
    @Expose()
    id: number

    @Expose()
    content: string;

    @Expose()
    commentOn: Date

    @Expose()
    public singlePost: SinglePost

    @Expose()
    public user: User

    @Exclude()
    public isDeleted: boolean
}