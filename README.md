# Forum-a Project

![alt text](./backend/resources/home-page.jpg)

Used Technologies: TypeScript, Node.js, NestJS, TypeORM, MySQL, Angular, Angular Material UI


# Public part - functionalities
* Mr Smiley's random interesting facts
* Most popular topics
* Observe most popular topics and its comments
* Sign in with validations
* Log in with validations

# Private part - functionalities
![alt text](./backend/resources/post-with-comments.jpg)

* Create topic/comment
* Topic's categories
* Edit your topic/comment
* Like topic/comment
* Observe users liked this topic/comment
* Delete your topic/comment
* Flag topic
* Add topic to favourites
* Observe your favourites topics
* Search topic/comment/user
* Observe your chronologic activity (Profile
* Observe your friends
* Add friend/Remove from friends

# Admin part - functionalities
![alt text](./backend/resources/admin.jpg)

* Make user admin
* Approve/delete flagged topics
* Ban user for period and automaticly unban after this period

# Installation
1. Clone the repo
2. npm install in both frontend and backend
3. run mysql database and make schema forum_db
4. in backend run npm run start:dev to create entities
5. enter your database options in backend/database/seed
 

 example


    { 
         type: "mysql",
         host: "localhost",
         port: 3306,
         username: "root",
         password: "root",
         database: "forum_db",
         entities: [join(__dirname, '/../../**/**.entity{.ts,.js}')]
    }

6. npm run seed
7. in frontend ng serve
8. open in browser http://localhost:4200
9. npm run test - for the tests
10. Enjoy

